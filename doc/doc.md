OData
   
   
   OData是 Open data Protocol 的缩写。是微软提供的一种web数据访问协议。提供标准的CRUD操作进行查询和操作数据集

OData继承了webapi的特性，并进行扩展。 

通过odata客户端代理的使用，
1.	有类似ef的上下文状态跟踪
2.	通过使用客户端代理，有种wcf的感觉
3.	Api貌似统一了他们，
Ps: restful开发模式使用过程中，对restful接口调用，鉴于api参数传递，以及认证，通常需要对api进行一次封装，以便调用，该场景与OData的类似，odata
的实体上下文状态跟踪与ef的类似




查询
$expand   查询相关的实体，一般是有导航关系的。指定后可将导航关系先关实体加载出来
$select    选择实体内指定字段，返回指定字段记录集合—类似sql的select
$value     获取到实体属性的值
$filter      过滤条件，后面的过滤条件类似linq的条件
$inlinecount  返回记录集的记录总数
$skip  跳过
$top  前多个个记录
$orderby 排序

(查询： 1. 可以用top-skip指定分页大小，2，可以在服务端查询数据集上指定查询分页大小([Queryable(PageSize=10)]) 3, 需要查询的需要指定 [Queryable]属性，4，可以指定查询属性，5，查询参数可以直接当作方法的参数，而不用写查询属性)



/odata/Products(1)/Name 
/odata/Products(1)/Name/$value

/odata/Products?$select=Name,Supplier&$expand=Supplier
/odata/Products?$select=Price,Name 
/odata/Categories(1)?$expand=Products/Supplier

/Products?$top=10&$skip=20
/Products?$filter=Category eq 'Toys'
/Products?$filter=Price lt 10
/Products?$filter=Price ge 5 and Price le 15
/Products?$filter=substringof('zz',Name)
/Products?$filter=year(ReleaseDate) gt 2005
/Products?$inlinecount=allpages
/Products?$orderby=Name

/Products?$orderby=Price
/Products?$orderby=Price desc
/Products?$orderby=Category,Price desc

[Queryable(AllowedQueryOptions=
    AllowedQueryOptions.Skip | AllowedQueryOptions.Top)]



http://www.asp.net/web-api/overview/odata-support-in-aspnet-web-api/supporting-odata-query-options



OData Client

https://github.com/object/Simple.OData.Client.git

