﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace OData.Silverlight
{
    public class CountEventArgs : EventArgs
    {
        public int TotalCount { get; set; }
    }
    public interface IDataSource<TEntity> where TEntity : class
    {
        /// <summary>
        /// This method is called to sort the SourceCollection on the AstoriaCollection
        /// </summary>
        /// <param name="sortDescriptions">A collection that contains the SortDescriptions</param>
        void Sort(SortDescriptionCollection sortDescriptions);
        /// <summary>
        /// This method is called to load the <paramref name="pageIndex"/> page of the SourceCollection
        /// </summary>
        /// <param name="pageIndex">The page index for the new page of results that the collection should fetch</param>
        void GoToPage(int pageIndex);
        /// <summary>
        /// This method is called to calculate the TotalCount of entities in the SourceCollection
        /// </summary>
        void GetTotalCount();
        event EventHandler<CountEventArgs> GetTotalCountCompleted;
    }
}

