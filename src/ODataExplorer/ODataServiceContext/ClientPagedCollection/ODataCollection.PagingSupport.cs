﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace OData.Silverlight
{
    public partial class AstoriaCollection<TEntity>
    {
        #region IPagedCollectionView Members

        private bool _canChangePage = true;
        public bool CanChangePage
        {
            get { return _canChangePage; }
            protected set
            {
                _canChangePage = value;
                RaisePropertyChanged("CanChangePage");
            }
        }

        private bool _IsPageChanging;
        public bool IsPageChanging
        {
            get { return _IsPageChanging; }
            protected set
            {
                _IsPageChanging = value;
                RaisePropertyChanged("IsPageChanging");
            }
        }

        public int ItemCount
        {
            get { return SourceList.Count; }
        }

        public bool MoveToFirstPage()
        {

            MoveToPageInternal(0);
            return true;
        }
        public bool MoveToLastPage()
        {
            return false;
        }
        protected virtual void MoveToPageInternal(int nextPageIndex)
        {
            if (PageChanging != null)
            {
                this.PageChanging(this, new PageChangingEventArgs(nextPageIndex));
            }
            PageIndex = nextPageIndex;
            _dataSource.GoToPage(nextPageIndex);
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new EventArgs());
            }
        }
        public bool MoveToNextPage()
        {
            MoveToPageInternal(PageIndex + 1);
            return true;
        }

        public bool MoveToPage(int pageIndex)
        {
            MoveToPageInternal(pageIndex);
            return true;
        }

        public bool MoveToPreviousPage()
        {
            if (PageIndex > 0)
            {
                MoveToPageInternal(PageIndex - 1);
                return true;
            }
            else
            {
                return false;
            }
        }

        public event EventHandler<EventArgs> PageChanged;

        public event EventHandler<PageChangingEventArgs> PageChanging;

        private int _PageIndex = 0;
        public int PageIndex
        {
            get { return _PageIndex; }
            protected set
            {
                _PageIndex = value;
                RaisePropertyChanged("PageIndex");
            }
        }
        private int _PageSize;
        public int PageSize
        {
            get { return _PageSize; }
            set
            {
                _PageSize = value;
                RaisePropertyChanged("PageSize");
            }
        }

        private int _TotalItemCount;
        private bool _hasCounted = false;
        public int TotalItemCount
        {
            get
            {
                if (!_hasCounted)
                {
                    _dataSource.GetTotalCount();
                    _hasCounted = true;
                }
                return _TotalItemCount;
            }
            private set
            {
                _hasCounted = true;
                _TotalItemCount = value;
                RaisePropertyChanged("TotalItemCount");
            }
        }

        #endregion
    }
}
