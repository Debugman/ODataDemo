﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System;
using System.Data.Services.Common;
using System.Collections.Generic;
using System.ComponentModel;

namespace OData.Silverlight
{


    [DataServiceEntity]
    public class ODataEntity : INotifyPropertyChanged
    {
        public string OData_Atom { get; set; }
        public EntityType EntityType { get; set; }
        public string MediaUri { get; set; }
        public string MediaType { get; set; }
        public Dictionary<string, Uri> NavigationProperties { get; set; }
        public ODataEntity()
        {
            this.Columns = new Dictionary<string, object>();
            this.NavigationProperties = new Dictionary<string, Uri>();
        }
        public object this[string Name]
        {
            get
            {
                if (this.Columns.ContainsKey(Name))
                {
                    return this.Columns[Name];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.Columns[Name] = value;
                RaisePropertyChanged(Name);
            }
        }
        public Dictionary<string, object> Columns { get; set; }

        #region INotifyPropertyChanged Members

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreODataSerializeAttribute : Attribute
    {
    }
}
