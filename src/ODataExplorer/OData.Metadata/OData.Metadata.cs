﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/

using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Net.Browser;

namespace OData.Silverlight
{
    public abstract class MetadataType
    {
    }
    public class EntityKey : MetadataType
    {
        public List<string> KeyNames { get; set; }
    }
    public class Parameter : MetadataType
    {
        public string Name { get; set; }
        public Type Type { get; set; }
    }
    public class FunctionImport : MetadataType
    {
        public string EntitySetName { get; set; }
        public string Name { get; set; }
        public Type ReturnType { get; set; }
        public string HttpMethod { get; set; }
        public List<Parameter> Parameters { get; set; }
    }

    public class ComplexType : MetadataType
    {
    }
    public class EntityType : MetadataType
    {
        public string Name { get; set; }
        public string BaseType { get; set; }
        public bool HasStream { get; set; }
        public bool HasComplexTypeProperties
        {
            get
            {
                return this.Properties.Any(property => property.PropertyType == null);
            }
        }
        public List<string> KeyProperties { get; set; }
        public List<EntityPropertyMapping> FeedCustomizationMappings
        {
            get
            {
                return this.Properties.Where(prop => prop.FeedCustomizationMapping != null).Select(prop => prop.FeedCustomizationMapping).ToList();
            }
        }
        public List<EntityProperty> Properties { get; set; }
        public List<NavigationProperty> Navigations { get; set; }
    }
    public class EntitySet : MetadataType
    {
        public string SetName { get; set; }
        public string TypeName { get; set; }
    }
    public class EntitySet2 : MetadataType
    {
        public string LinkName { get; set; }
        public string SetName { get; set; }
    }
    public class EntityProperty : MetadataType
    {
        public Type PropertyType { get; set; }
        public string PropertyName { get; set; }
        public string GetValidationString()
        {
            return String.Empty;
        }
        public EntityPropertyMapping FeedCustomizationMapping { get; set; }
        public bool IsConcurrencyToken { get; set; }
        public string MaxLength { get; set; }
        public string DefaultValue { get; set; }
    }
    public class EntityPropertyRef : MetadataType
    {
        public string ReferredPropertyName { get; set; }
    }
    public class Association : MetadataType
    {
        public string Name { get; set; }
        public List<End> Ends { get; set; }
    }
    public enum Multiplicity
    {
        One,
        Many,
        ZeroOrOne,
        ZeroOrMany
    }
    public class End : MetadataType
    {
        public string Role { get; set; }
        public string Type { get; set; }
        public Multiplicity Multiplicity { get; set; }
        public bool CascadeOnDelete { get; set; }
    }
    public class AssociationSet : MetadataType
    {
        public string AssociationName { get; set; }
        public Association Association { get; set; }
        public string Name { get; set; }
    }
    public class NavigationProperty : Association
    {
        public string TypeName { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public bool IsMultiple { get; set; }
        public string Relationship { get; set; }
    }
    public enum EPMMappings
    {
        Title,
        Summary,
        Updated,
        AuthorName,
        Published
    }
    public enum MediaType
    {
        Image,
        Document,
        None
    }
    public class EntityPropertyMapping : MetadataType
    {
        public EPMMappings Mapping { get; set; }

    }
    public class Schema : MetadataType
    {
        public string Namespace { get; set; }
        public List<EntityContainer> Containers { get; set; }
        public List<EntityType> EntityTypes { get; set; }
        public List<Association> Associations { get; set; }
    }
    public class DataService : MetadataType
    {
        public string Name { get; set; }
        public List<Schema> Schemas { get; set; }
    }
    public class EDMModel : MetadataType
    {
        public string Name { get; set; }
        public ServiceDocument ServiceDocument { get; set; }
        public string DataServiceUri { get; set; }
        public string MetadataUri { get; set; }
        public List<DataService> DataServices { get; set; }

        public EntityContainer FirstContainerWithSets
        {
            get
            {
                if (this.DataServices != null)
                {
                    return this.ContainersWithSets.FirstOrDefault();
                }
                return null;
            }
        }
        public List<EntityContainer> ContainersWithSets
        {
            get
            {
                if (this.DataServices != null)
                {
                    return (from dataService in this.DataServices
                            from schema in dataService.Schemas
                            from container in schema.Containers
                            where container.EntitySets != null
                            && container.EntitySets.Count > 0
                            select container).ToList();
                }
                return null;
            }
        }
        public EntitySet GetEntitySet(string setName)
        {
            return (from dataService in this.DataServices
                    from schema in dataService.Schemas
                    from container in schema.Containers
                    from eSet in container.EntitySets
                    where eSet.SetName == setName
                    select eSet).FirstOrDefault();
        }
        public EntityType GetEntityType(EntitySet entitySet)
        {
            string typeName = entitySet.TypeName.Substring(entitySet.TypeName.LastIndexOf('.') + 1);
            return (from dataService in this.DataServices
                    from schema in dataService.Schemas
                    from entityType in schema.EntityTypes
                    where entityType.Name == typeName
                    select entityType).FirstOrDefault();
        }

        public EntitySet GetEntitySet(EntityType entityType)
        {
            string typeName = entityType.Name.Substring(entityType.Name.LastIndexOf('.') + 1);
            var entitySet = (from dataService in this.DataServices
                             from schema in dataService.Schemas
                             from container in schema.Containers
                             from set in container.EntitySets
                             where set.TypeName == typeName
                             select set).FirstOrDefault();
            return entitySet;
        }

        public EntityType GetEntityType(string entityTypeName)
        {
            EntityType eType =
             (from dataService in this.DataServices
              from schema in dataService.Schemas
              from entityType in schema.EntityTypes
              where entityType.Name == entityTypeName
              select entityType).FirstOrDefault();
            if (eType == null)
            {
                if (entityTypeName.ToLower().EndsWith("s"))
                {
                    return this.GetEntityType(entityTypeName.TrimEnd('s'));
                }
            }
            return eType;
        }
    }
    public class EntityContainer : MetadataType
    {
        public List<EntitySet> EntitySets { get; set; }
        public List<FunctionImport> Functions { get; set; }
        public string Name { get; set; }
    }
    public class AsyncParseResult : IAsyncResult
    {
        #region IAsyncResult Members

        private object asyncState;
        public object AsyncState
        {
            get { return asyncState; }
            set { asyncState = value; }
        }

        public System.Threading.WaitHandle AsyncWaitHandle
        {
            get { return null; }
        }

        public bool CompletedSynchronously
        {
            get { throw new NotImplementedException(); }
        }

        private bool isCompleted;
        public bool IsCompleted
        {
            get { return isCompleted; }
            set { isCompleted = value; }
        }

        #endregion
    }
    public class MetadataParser
    {
        public delegate void OnParseComplete(EDMModel model, Exception Error);
        public event OnParseComplete ParseComplete;

        private void RaiseParseComplete(EDMModel model, Exception Error)
        {
            if (this.ParseComplete != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(
                    () =>
                    {
                        ParseComplete(model, Error);
                    }
                    );
            }
        }
        private string serviceUri;
        public void BeginParse(string ServiceUri)
        {
            serviceUri = ServiceUri;
            if (!String.IsNullOrEmpty(ServiceUri))
            {
#if SILVERLIGHT
                WebRequest.RegisterPrefix(ServiceUri, WebRequestCreator.BrowserHttp);
                HttpWebRequest webRequest = WebRequest.Create(ServiceUri) as HttpWebRequest;
                webRequest.BeginGetResponse(
                    (asResult) =>
                    {
                        try
                        {
                            HttpWebResponse webResponse = webRequest.EndGetResponse(asResult) as HttpWebResponse;
                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                XDocument xDocMetadataDocument = XDocument.Load(webResponse.GetResponseStream());
                                EDMModel model = parseInternal(xDocMetadataDocument.Root) as EDMModel;
                                RaiseParseComplete(model, null);
                            }
                            else
                            {
                                RaiseParseComplete(null, new Exception("Failure downloading service metadata"));
                            }
                        }
                        catch (Exception errorInDownloadingMetadata)
                        {

                            RaiseParseComplete(null, errorInDownloadingMetadata);
                        }
                    }, null);

#else
                System.Xml.XmlReaderSettings settings = new System.Xml.XmlReaderSettings();
                System.Xml.XmlResolver resolver = new System.Xml.XmlUrlResolver();
                if (!String.IsNullOrEmpty(userName) && !String.IsNullOrEmpty(passWord))
                {
                    resolver.Credentials = new System.Net.NetworkCredential(userName, passWord);
                }
                else
                {
                    resolver.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                }

                settings.XmlResolver = resolver;
            }
            System.Xml.XmlReader reader = System.Xml.XmlReader.Create(ServiceUri, settings);
                XDocument xDocMetadataDocument = XDocument.Load(reader);
              

#endif
            }
        }
        XName xnName = XName.Get("Name");
        XName xnConcurrencyMode = XName.Get("ConcurrencyMode");
        XName xnDefaultValue = XName.Get("DefaultValue");
        XName xnOnDelete = XName.Get("OnDelete");
        XName xnAction = XName.Get("Action");
        XName xnType = XName.Get("Type");
        XName xnRole = XName.Get("Role");
        XName xnFromRole = XName.Get("FromRole");
        XName xnToRole = XName.Get("ToRole");
        XName xnRelationShip = XName.Get("Relationship");
        XName xnMaxLength = XName.Get("MaxLength");
        XName xnMultiplicity = XName.Get("Multiplicity");
        XName xnEntityType = XName.Get("EntityType");
        XName xnReturnType = XName.Get("ReturnType");
        XName xnHttpMethod = XName.Get("HttpMethod");
        XName xnEntitySet = XName.Get("EntitySet");
        XName xnNameSpace = XName.Get("Namespace");
        XName xnBaseType = XName.Get("BaseType");
        XName xnHasStream = XName.Get("HasStream", "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata");
        XName xnKeepInContent = XName.Get("FC_KeepInContent", "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata");
        XName xnTargetPath = XName.Get("FC_TargetPath", "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata");

        private MetadataType parseInternal(XElement element)
        {
            switch (element.Name.LocalName)
            {
                case "Edmx":
                    EDMModel model = new EDMModel()
                    {
                        MetadataUri = serviceUri,
                        DataServices = new List<DataService>()
                    };
                    foreach (XElement dataServiceElement in element.Elements())
                    {
                        DataService dService = parseInternal(dataServiceElement) as DataService;
                        model.DataServices.Add(dService);
                    }
                    return model;
                case "DataServices":
                    DataService dataService = new DataService()
                    {
                        Schemas = new List<Schema>()
                    };
                    foreach (XElement schemaElement in element.Elements())
                    {
                        Schema schemaEl = parseInternal(schemaElement) as Schema;
                        dataService.Schemas.Add(schemaEl);
                    }
                    //Fixup Navigation properties
                    var navProperties = from schema in dataService.Schemas
                                        from eType in schema.EntityTypes
                                        from navs in eType.Navigations
                                        select new { schema, navs, eType };
                    foreach (var navProp in navProperties)
                    {
                        var association = (from schema in dataService.Schemas
                                           from associations in schema.Associations
                                           where associations.Name == navProp.navs.Relationship.Replace(navProp.schema.Namespace + ".", "")
                                           select new { schema, associations }).FirstOrDefault();
                        if (association != null)
                        {
                            End end = association.associations.Ends.FirstOrDefault(en => en.Type.Replace(association.schema.Namespace + ".", "") == navProp.eType.Name);
                            End otherEnd = association.associations.Ends.FirstOrDefault(e => e != end);
                            navProp.navs.Ends = new List<End>();
                            navProp.navs.Ends.Add(end);
                            navProp.navs.Ends.Add(otherEnd);
                            navProp.navs.IsMultiple = otherEnd.Multiplicity == Multiplicity.Many;
                            navProp.navs.TypeName = otherEnd.Type.Replace(association.schema.Namespace + ".", "");
                        }
                    }
                    return dataService;
                case "Schema":
                    Schema _edmSchema = new Schema()
                    {
                        Namespace = element.Attribute(xnNameSpace).Value.ToString(),
                        EntityTypes = new List<EntityType>(),
                        Containers = new List<EntityContainer>(),
                        Associations = new List<Association>()
                    };
                    foreach (XElement entityElement in element.Elements())
                    {
                        MetadataType mType = parseInternal(entityElement) as MetadataType;
                        if (mType is EntityType)
                        {
                            _edmSchema.EntityTypes.Add(mType as EntityType);
                        }
                        else if (mType is EntityContainer)
                        {
                            _edmSchema.Containers.Add(mType as EntityContainer);
                        }
                        else if (mType is Association)
                        {
                            _edmSchema.Associations.Add(mType as Association);
                        }
                    }
                    return _edmSchema;
                case "EntitySet":
                    string typeName = element.Attribute(xnEntityType).Value.ToString();
                    return new EntitySet()
                    {
                        SetName = element.Attribute(xnName).Value.ToString(),
                        TypeName = typeName.Substring(typeName.IndexOf(".") + 1)
                    };
                case "EntityType":
                    EntityType type = new EntityType()
                    {
                        Name = element.Attribute(xnName).Value.ToString(),
                        BaseType = element.Attribute(xnBaseType) != null ? element.Attribute(xnBaseType).Value.ToString() : String.Empty,
                        HasStream = element.Attribute(xnHasStream) != null ? Boolean.Parse(element.Attribute(xnHasStream).Value) : false,
                        Navigations = new List<NavigationProperty>()
                    };
                    foreach (XElement propertyElement in element.Elements())
                    {
                        if (type.Properties == null)
                        {
                            type.Properties = new List<EntityProperty>();
                        }
                        MetadataType mType = parseInternal(propertyElement);
                        if (mType is EntityProperty)
                        {
                            EntityProperty eProperty = mType as EntityProperty;
                            type.Properties.Add(eProperty);
                        }
                        else if (mType is EntityKey)
                        {
                            EntityKey key = mType as EntityKey;
                            type.KeyProperties = key.KeyNames;
                        }
                        else if (mType is NavigationProperty)
                        {
                            type.Navigations.Add(mType as NavigationProperty);
                        }
                    }
                    return type;
                case "Key":
                    EntityKey keyProperty = new EntityKey()
                    {
                        KeyNames = new List<string>()
                    };
                    foreach (XElement keyPropertyElement in element.Elements())
                    {
                        EntityPropertyRef propertyRef = parseInternal(keyPropertyElement) as EntityPropertyRef;
                        keyProperty.KeyNames.Add(propertyRef.ReferredPropertyName);
                    }
                    return keyProperty;
                case "PropertyRef":
                    return new EntityPropertyRef()
                    {
                        ReferredPropertyName = element.Attribute(xnName).Value.ToString()
                    };
                case "Property":
                    EntityProperty property =
                    new EntityProperty()
                    {
                        PropertyName = element.Attribute(xnName).Value.ToString(),
                        MaxLength = element.Attribute(xnMaxLength) != null ? element.Attribute(xnMaxLength).Value.ToString() : String.Empty,
                        PropertyType = EDMUtility.ConvertEdmTypeToCsharpType(element.Attribute(xnType).Value.ToString()),
                        IsConcurrencyToken = element.Attribute(xnConcurrencyMode) != null ? element.Attribute(xnConcurrencyMode).Value.ToString() == "Fixed" : false,
                        DefaultValue = element.Attribute(xnDefaultValue) != null ? element.Attribute(xnDefaultValue).Value.ToString() : String.Empty
                    };
                    if (element.Attribute(xnKeepInContent) != null && !Boolean.Parse(element.Attribute(xnKeepInContent).Value))
                    {
                        property.FeedCustomizationMapping = new EntityPropertyMapping()
                        {
                            Mapping = EDMUtility.GetMappingInfo(element.Attribute(xnTargetPath).Value)
                        };
                    }
                    return property;
                case "NavigationProperty":

                    return new NavigationProperty()
                    {
                        Name = element.Attribute(xnName).Value.ToString(),
                        Relationship = element.Attribute(xnRelationShip).Value.ToString(),
                        From = element.Attribute(xnFromRole).Value.ToString(),
                        To = element.Attribute(xnToRole).Value.ToString(),
                    };
                case "FunctionImport":
                    var function = new FunctionImport()
                    {
                        Name = element.Attribute(xnName).Value.ToString(),
                        EntitySetName = element.Attribute(xnEntitySet) != null ? element.Attribute(xnEntitySet).Value.ToString() : String.Empty,
                        ReturnType = element.Attribute(xnReturnType) != null ? EDMUtility.ConvertEdmTypeToCsharpType(element.Attribute(xnReturnType).Value.ToString()) : null,
                        HttpMethod = element.Attribute(xnHttpMethod) != null ? element.Attribute(xnHttpMethod).Value.ToString() : "GET",
                    };
                    function.Parameters = new List<Parameter>();
                    foreach (XElement parameterElement in element.Elements())
                    {
                        MetadataType mType = parseInternal(parameterElement);
                        if (mType is Parameter)
                        {
                            function.Parameters.Add(mType as Parameter);
                        }
                    }
                    return function;
                case "Parameter":
                    return new Parameter()
                    {
                        Name = element.Attribute(xnName).Value.ToString(),
                        Type = EDMUtility.ConvertEdmTypeToCsharpType(element.Attribute(xnType).Value.ToString())
                    };
                case "EntityContainer":
                    EntityContainer eContainer = new EntityContainer()
                    {
                        Name = element.Attribute(xnName).Value.ToString(),
                        EntitySets = new List<EntitySet>(),
                        Functions = new List<FunctionImport>()
                    };
                    foreach (XElement setElement in element.Elements())
                    {
                        MetadataType mType = parseInternal(setElement) as MetadataType;
                        if (mType is EntitySet)
                        {
                            eContainer.EntitySets.Add(mType as EntitySet);
                        }
                        else if (mType is FunctionImport)
                        {
                            eContainer.Functions.Add(mType as FunctionImport);
                        }
                    }
                    return eContainer;
                case "Association":
                    Association assoc = new Association()
                    {
                        Ends = new List<End>(),
                        Name = element.Attribute(xnName).Value.ToString()
                    };
                    foreach (XElement endElement in element.Elements())
                    {
                        MetadataType mType = parseInternal(endElement);
                        if (mType is End)
                        {
                            assoc.Ends.Add(mType as End);
                        }
                    }
                    return assoc;
                case "End":
                    Multiplicity endMult = Multiplicity.One;
                    switch (element.Attribute(xnMultiplicity).Value.ToString())
                    {
                        case "1":
                            endMult = Multiplicity.One;
                            break;
                        case "*":
                            endMult = Multiplicity.Many;
                            break;
                    }
                    return new End()
                    {
                        Role = element.Attribute(xnRole).Value.ToString(),
                        Type = element.Attribute(xnType).Value.ToString(),
                        CascadeOnDelete = element.Element(xnOnDelete) != null && element.Element(xnOnDelete).Attribute(xnAction).Value.ToString() == "Cascade",
                        Multiplicity = endMult
                    };
                case "AssociationSet":
                    return new AssociationSet();
                case "ReferentialConstraint":
                    return new EntityProperty();
                case "ComplexType":
                    return new ComplexType();
                default:
                    throw new ArgumentOutOfRangeException(element.Name.LocalName);
            }
        }
    }

    public class ServiceDocument
    {
        public Dictionary<String, Uri> EntitySetUris { get; set; }
        public Uri BaseUri { get; set; }
    }
    public class SvcDocParser
    {
        public delegate void OnParseComplete(ServiceDocument serviceDocument, Exception Error);
        public event OnParseComplete ParseComplete;

        private void RaiseParseComplete(ServiceDocument serviceDocument, Exception Error)
        {
            if (this.ParseComplete != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(
                    () =>
                    {
                        ParseComplete(serviceDocument, Error);
                    }
                    );
            }
        }
        private string serviceUri;
        private Uri baseUri;
        public void BeginParse(string ServiceUri)
        {
            serviceUri = ServiceUri;
            if (!String.IsNullOrEmpty(ServiceUri))
            {
#if SILVERLIGHT
                WebRequest.RegisterPrefix(ServiceUri, WebRequestCreator.BrowserHttp);
                HttpWebRequest webRequest = WebRequest.Create(ServiceUri) as HttpWebRequest;
                webRequest.BeginGetResponse(
                    (asResult) =>
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(
                            () =>
                            {
                                try
                                {
                                    HttpWebResponse webResponse = webRequest.EndGetResponse(asResult) as HttpWebResponse;
                                    if (webResponse.StatusCode == HttpStatusCode.OK)
                                    {
                                        XDocument xDocMetadataDocument = XDocument.Load(webResponse.GetResponseStream());
                                        parseInternal(xDocMetadataDocument.Root);
                                        RaiseParseComplete(new ServiceDocument() { EntitySetUris = entitySetLinks, BaseUri = baseUri }, null);
                                    }
                                    else
                                    {
                                        RaiseParseComplete(null, new Exception("Failure downloading service document"));
                                    }
                                }
                                catch (Exception errorInDownloadingMetadata)
                                {
                                    String userMessage =
                                        String.Format(
                                        "We failed to download the Service Document.\r\n Here is the URI we tried : {0} .\r\n Please check the Data Service Uri by trying it in a browser"
                                        , this.serviceUri
                                        );
                                    RaiseParseComplete(null, new Exception(userMessage));
                                }
                            }
                            );
                    }, null);

#else
                System.Xml.XmlReaderSettings settings = new System.Xml.XmlReaderSettings();
                System.Xml.XmlResolver resolver = new System.Xml.XmlUrlResolver();
                if (!String.IsNullOrEmpty(userName) && !String.IsNullOrEmpty(passWord))
                {
                    resolver.Credentials = new System.Net.NetworkCredential(userName, passWord);
                }
                else
                {
                    resolver.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                }

                settings.XmlResolver = resolver;
            }
            System.Xml.XmlReader reader = System.Xml.XmlReader.Create(ServiceUri, settings);
                XDocument xDocMetadataDocument = XDocument.Load(reader);
              

#endif
            }
        }

        XName xnHref = XName.Get("href");
        XName xnBase = XName.Get("base", "http://www.w3.org/XML/1998/namespace");
        XName xnTitle = XName.Get("title", "http://www.w3.org/2005/Atom");

        Dictionary<string, Uri> entitySetLinks = new Dictionary<string, Uri>();

        private void parseInternal(XElement element)
        {
            switch (element.Name.LocalName)
            {
                case "service":
                    DataService dService = new DataService();
                    baseUri = new Uri(element.Attribute(xnBase).Value, UriKind.Absolute);

                    foreach (XElement workspaceElement in element.Elements())
                    {
                        parseInternal(workspaceElement);
                    }
                    break;
                case "workspace":
                    EntitySet2 entitySetWithHref = new EntitySet2();
                    int count = 0;
                    foreach (XElement collectionElement in element.Elements())
                    {
                        if (count != 0)
                        {
                            parseInternal(collectionElement);
                        }
                        count++;
                    }
                    break;
                case "collection":
                    IEnumerable<XElement> tempIEnum = element.Elements();
                    XElement titleElement = element.Element(xnTitle);

                    //get the href next
                    string hrefVal = element.Attribute(xnHref).Value;
                    Uri hrefUri = new Uri(String.Format("{0}/{1}", baseUri.OriginalString.TrimEnd('/'), hrefVal));

                    entitySetLinks.Add(titleElement.Value, hrefUri);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(element.Name.LocalName);
            }
        }
    }

    public static class EDMUtility
    {
        public static MediaType GetMediaType(string mediaTypeString)
        {
            if (String.IsNullOrEmpty(mediaTypeString))
            {
                return MediaType.None;
            }
            switch (mediaTypeString)
            {
                case "image/jpeg":
                case "image/png":
                case "image/gif":
                case "image/bmp":
                    return MediaType.Image;
                default:
                    return MediaType.Document;
            }
        }
        public static EPMMappings GetMappingInfo(string syndicationItemName)
        {
            switch (syndicationItemName)
            {
                case "SyndicationTitle":
                    return EPMMappings.Title;
                case "SyndicationSummary":
                    return EPMMappings.Summary;
                case "SyndicationAuthorName":
                    return EPMMappings.AuthorName;
                case "SyndicationPublished":
                    return EPMMappings.Published;
                case "SyndicationUpdated":
                    return EPMMappings.Updated;
            }
            throw new InvalidOperationException();
        }
        private static readonly Dictionary<string, Type> edmTypes = new Dictionary<string, Type>
	                                                              	{
	                                                              		{"Edm.Binary", typeof(byte[])},
	                                                              		{"Edm.Boolean", typeof(bool)},
	                                                              		{"Edm.DateTime", typeof(DateTime)},
	                                                              		{"Edm.Single", typeof(Single)},
	                                                              		{"Edm.Double", typeof(double)},
	                                                              		{"Edm.Decimal", typeof(double)},
	                                                              		{"Edm.Int32", typeof(Int32)},
	                                                              		{"Edm.Int64", typeof(long)},
	                                                              		{"Edm.Int16", typeof(short)},
	                                                              		{"Edm.Guid", typeof(Guid)},
	                                                              		{"Edm.String", typeof(string)}
	                                                              	};
        public static string GetLiteralForm(Parameter parameter, int index)
        {
            string literalForm = String.Empty;
            switch (parameter.Type.Name.ToLower())
            {
                case "string":
                    literalForm = String.Format("'{{{0}}}'", index);
                    break;
                case "int32":
                    literalForm = String.Format("{{{0}}}", index);
                    break;
            }
            return literalForm;
        }
        public static Type ConvertEdmTypeToCsharpType(string edmType)
        {
            if (edmTypes.ContainsKey(edmType))
            {
                Type eType = edmTypes[edmType];
                return eType;
            }
            else
            {
                return null;
            }
        }
    }
}
