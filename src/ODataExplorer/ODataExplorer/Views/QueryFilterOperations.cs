﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System;
using System.Collections.Generic;

namespace SL_OData_Explorer
{
    public class QueryFilterOperation
    {
        public Func<string, string, string> Encode { get; set; }
        public static List<QueryFilterOperation> StringFilters
        {
            get
            {
                return new List<QueryFilterOperation>()
                {
                    new QueryFilterOperation(){ 
                        Display ="Contains", 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("substringof('{0}',{1})", propertyValue, propertyName);
                        }
                    }, 
                    new QueryFilterOperation(){ Display ="StartsWith" ,
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("startswith('{0}',{1})", propertyValue, propertyName);
                        }
                    },
                    new QueryFilterOperation(){ Display ="Equals"  ,
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} eq '{1}'", propertyName, propertyValue );
                        }},
                    new QueryFilterOperation(){ Display ="EndsWith"  ,
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("endswith('{0}',{1})", propertyValue, propertyName);
                        }  
                    }
                };
            }
        }

        public static List<QueryFilterOperation> IntFilters
        {
            get
            {
                return new List<QueryFilterOperation>()
                {
                    new QueryFilterOperation(){ Display ="Greater than or equal to" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} ge {1}", propertyName, propertyValue);
                        }},
                    new QueryFilterOperation(){ Display ="Greater than" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} gt {1}", propertyName, propertyValue);
                        }}, 
                    new QueryFilterOperation(){ Display ="Equal To" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} eq {1}", propertyName, propertyValue);
                        }}, 
                    new QueryFilterOperation(){ Display ="Lesser than" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} lt {1}", propertyName, propertyValue);
                        }},
                    new QueryFilterOperation(){ Display ="Lesser than or equal to" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} le {1}", propertyName, propertyValue);
                        }} 
                };
            }
        }


        public static List<QueryFilterOperation> DateFilters
        {
            get
            {
                Func<string, string> EncodeDateTime = (dateTimeString) =>
                {
                    return String.Format("datetime'{0}'", DateTime.Parse(dateTimeString).ToString("yyyy-MM-ddThh:mm:ss"));
                };
                return new List<QueryFilterOperation>()
                {

                    new QueryFilterOperation(){ Display ="Greater than or equal to" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} ge {1}", propertyName, EncodeDateTime(propertyValue) );
                        }},
                    new QueryFilterOperation(){ Display ="Greater than" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} gt {1}", propertyName,EncodeDateTime(propertyValue));
                        }}, 
                    new QueryFilterOperation(){ Display ="Equal To" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} eq {1}", propertyName,EncodeDateTime(propertyValue));
                        }}, 
                    new QueryFilterOperation(){ Display ="Lesser than" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} lt {1}", propertyName,EncodeDateTime(propertyValue));
                        }},
                    new QueryFilterOperation(){ Display ="Lesser than or equal to" , 
                        Encode =(propertyName,propertyValue)=>{
                            return String.Format("{0} le {1}", propertyName, EncodeDateTime(propertyValue));
                        }}
                };
            }
        }
        public string Display { get; set; }
    }

}
