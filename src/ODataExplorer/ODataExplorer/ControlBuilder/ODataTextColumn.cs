﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/

using System;
using System.Windows;
using System.Windows.Controls;

namespace SL_OData_Explorer
{
    public class ODataTextColumn : DataGridBoundColumn
    {

        protected override FrameworkElement GenerateEditingElement(DataGridCell cell, object dataItem)
        {
            throw new NotImplementedException();
        }

        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            TextBlock textElement = new TextBlock();
            textElement.MaxWidth = 400;
            textElement.Margin = new Thickness(3);
            textElement.TextWrapping = TextWrapping.NoWrap;
            textElement.SetBinding(TextBlock.TextProperty, this.Binding);
            TextBlock tooltipText = new TextBlock();
            tooltipText.MaxWidth = 500;
            tooltipText.TextWrapping = TextWrapping.Wrap;
            tooltipText.SetBinding(TextBlock.TextProperty, this.Binding);
            ToolTipService.SetToolTip(textElement, tooltipText);
            return textElement;
        }

        protected override object PrepareCellForEdit(FrameworkElement editingElement, RoutedEventArgs editingEventArgs)
        {
            throw new NotImplementedException();
        }
    }
}
