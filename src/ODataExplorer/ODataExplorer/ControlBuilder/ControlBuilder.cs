﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using OData.Silverlight;
using System.Windows.Data;
using System.Windows.Markup;
using System.Text;

namespace SL_OData_Explorer
{
    public class ControlBuilder
    {
        public static DataGridColumn GetColumnTemplate(NavigationProperty property, IValueConverter converter,
            Action<ODataEntity, NavigationProperty> OnEntitySelected)
        {
            return new ODataNavigationPropertyColumn()
            {
                Header = property.Name,
                Association = property,
                OnEntitySelected = OnEntitySelected,
                Binding = new Binding()
            };
        }

        public static DataGridColumn GetEditColumnTemplate(ODataServiceContext serviceContext)
        {
            return new ODataEntityEditColumn(serviceContext)
            {
                Header = "Edit",
                Binding = new Binding()
            };
        }
        public static DataGridColumn GetMediaColumnTemplate(IValueConverter converter)
        {
            return new ODataMediaColumn()
            {
                Header = "Media",
                Binding = new Binding()
                {
                    Path = new PropertyPath("MediaUri")
                }
            };
        }

        public static Binding GetBinding(EntityProperty property, IValueConverter converter)
        {
            return new Binding()
                    {
                        Converter = converter,
                        ConverterParameter = property.PropertyName
                    };
        }
        public static DataGridColumn GetColumnTemplate(EntityProperty property, IValueConverter converter)
        {
            DataGridColumn dgColumn = null;
            if (property.PropertyType == typeof(DateTime))
            {
                dgColumn = new ODataTextColumn()
                {
                    Header = property.PropertyName,
                    SortMemberPath = property.PropertyName,
                    Binding = GetBinding(property, converter)
                };
            }
            else if (property.PropertyType == typeof(bool))
            {
                dgColumn = new DataGridCheckBoxColumn()
                {
                    Header = property.PropertyName,
                    Binding = GetBinding(property, converter)
                };
            }
            else
            {
                dgColumn = new ODataTextColumn()
                {
                    Header = property.PropertyName,
                    Binding = GetBinding(property, converter)
                };
            }
            return dgColumn;
        }
    }
}
