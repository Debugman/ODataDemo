﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System;
using System.Windows;
using System.Windows.Controls;
using OData.Silverlight;
using SL_OData_Explorer.Views;

namespace SL_OData_Explorer
{
    public class ODataEntityEditColumn : DataGridBoundColumn
    {
        public ODataServiceContext ServiceContext;
        public ODataEntityEditColumn(ODataServiceContext serviceContext)
        {
            ServiceContext = serviceContext;
            
        }
        protected override FrameworkElement GenerateEditingElement(DataGridCell cell, object dataItem)
        {
            return new TextBlock() { Text = " " };
        }

        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            HyperlinkButton clickHereButton = new HyperlinkButton();
            clickHereButton.Click += new RoutedEventHandler(clickHereButton_Click);
            clickHereButton.Content = "Edit";
            clickHereButton.SetBinding(HyperlinkButton.TagProperty, this.Binding);
            return clickHereButton;
        }

        void clickHereButton_Click(object sender, RoutedEventArgs e)
        {
            HyperlinkButton buttonClicked = sender as HyperlinkButton;
            if (buttonClicked != null && buttonClicked.Tag != null)
            {
                ODataEntity entity = buttonClicked.Tag as ODataEntity;
                ODataATOMDisplayWindow window = new ODataATOMDisplayWindow(entity, ServiceContext);
                window.Show();
            }

        }

        protected override object PrepareCellForEdit(FrameworkElement editingElement, RoutedEventArgs editingEventArgs)
        {
            throw new NotImplementedException();
        }

    }
}
