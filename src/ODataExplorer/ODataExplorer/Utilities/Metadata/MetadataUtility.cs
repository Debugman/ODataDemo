﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System;
using OData.Silverlight;
using SL_OData_Explorer.ViewModels;

namespace SL_OData_Explorer.Utilities
{
    public class MetadataUtility
    {
        private string DataServiceUri;
        public delegate void ParseCompleteDelegate(EDMModel model, Exception Error);
        public event ParseCompleteDelegate ParseComplete;
        private MetadataParser parser;
        private SvcDocParser svcParser;
        private string WorkspaceName;
        ServiceDocument serviceDocument;
        bool AddToWorkspaces = true;
        public void StartMetadataProcessing(string dataServiceUri, string workspaceName, bool addToWorkspaces)
        {
            AddToWorkspaces = addToWorkspaces;
            StartMetadataProcessing(dataServiceUri, workspaceName);
        }
        public void StartMetadataProcessing(string dataServiceUri, string workspaceName)
        {
            if (dataServiceUri.EndsWith("/$metadata"))
            {
                throw new ArgumentException("The Data Service Uri should be the service document , not the metadata endpoint");
            }

            DataServiceUri = dataServiceUri;
            WorkspaceName = workspaceName;

            svcParser = new SvcDocParser();
            svcParser.ParseComplete += new SvcDocParser.OnParseComplete(serviceDocumentParser_ParseComplete);
            svcParser.BeginParse(dataServiceUri);
        }

        void serviceDocumentParser_ParseComplete(ServiceDocument serviceDocument, Exception Error)
        {
            if (Error == null)
            {
                this.serviceDocument = serviceDocument;
                string dataServiceMetadataUri = String.Format("{0}/$metadata", DataServiceUri.TrimEnd('/'));
                parser = new MetadataParser();
                parser.ParseComplete += new MetadataParser.OnParseComplete(metadataParser_ParseComplete);
                parser.BeginParse(dataServiceMetadataUri);
            }
            else
            {
                if (ParseComplete != null)
                {
                    ParseComplete(null, Error);
                }
            }
        }

        void metadataParser_ParseComplete(EDMModel model, Exception Error)
        {
            if (Error == null)
            {
                App.MDViewModel.CurrentModel = model;
                model.ServiceDocument = serviceDocument;
                model.DataServiceUri = model.MetadataUri.Replace("/$metadata", "");
                if (AddToWorkspaces)
                {
                    ODataWorkspace newWorkspace = new ODataWorkspace()
                    {
                        Name = WorkspaceName,
                        Model = model
                    };

                    App.MDViewModel.Workspaces.Add(
                        newWorkspace
                        );
                }
            }
            if (this.ParseComplete != null)
            {
                this.ParseComplete(model, Error);
            }
        }
    }
}
