﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System.Windows;
using System.Windows.Controls;
using SL_OData_Explorer.Utilities;

namespace SL_OData_Explorer
{
    public partial class UseInformation : ChildWindow, IUserInfoWindow
    {
        public UseInformation()
        {
            InitializeComponent();
        }
        public UseInformation(string message)
            : this()
        {
            UserMessage = message;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #region IUserInfoWindow Members

        public string UserMessage
        {
            get
            {
                return txtUserMessage.Text;
            }
            set
            {
                txtUserMessage.Text = value;
            }
        }

        #endregion
    }
}

