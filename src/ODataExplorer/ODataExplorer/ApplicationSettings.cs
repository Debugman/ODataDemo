﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System.ComponentModel;

namespace SL_OData_Explorer
{
    public class ApplicationSettings : INotifyPropertyChanged
    {
        private static ApplicationSettings current;
        private int pageSize;
        private bool enablePaging;
        private bool showNavigationProperties = true;

        public static ApplicationSettings Current
        {
            get
            {
                return current;
            }
            set
            {
                current = value;
            }
        }
        public int PageSize { get { return 5; } }
        public bool EnablePaging { get { return true; } }
        public bool ShowNavigationProperties
        {
            get
            {
                return showNavigationProperties;
            }
            set
            {
                showNavigationProperties = value;
                RaisePropertyChanged("ShowNavigationProperties");
            }
        }


        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
