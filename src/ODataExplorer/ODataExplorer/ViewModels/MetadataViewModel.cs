﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System;
using System.Windows;
using OData.Silverlight;
using System.Collections.ObjectModel;

namespace SL_OData_Explorer.ViewModels
{
    public class ODataWorkspace : DependencyObject
    {
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public bool IsMetadataLoaded
        {
            get
            {
                return Model != null;
            }
        }


        // Using a DependencyProperty as the backing store for Name.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(ODataWorkspace), new PropertyMetadata("No name"));

        public EDMModel Model
        {
            get { return (EDMModel)GetValue(ModelProperty); }
            set { SetValue(ModelProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Model.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModelProperty =
            DependencyProperty.Register("Model", typeof(EDMModel), typeof(ODataWorkspace), new PropertyMetadata(null));
    }
    public class MetadataViewModel : DependencyObject
    {
        public Uri RequestURI { get; set; }
        public string EntitySetName { get; set; }
        public EntityType CurrentEntityType { get; set; }
        public delegate void OnModelChanged(EDMModel newWorkspace);
        public event OnModelChanged ModelSelectionChanged;
        public MetadataViewModel()
        {
            Workspaces = new ObservableCollection<ODataWorkspace>();
        }

        public EDMModel CurrentModel
        {
            get { return (EDMModel)GetValue(MyPropertyProperty); }
            set
            {
                if (ModelSelectionChanged != null)
                {
                    ModelSelectionChanged(value);
                }
                SetValue(MyPropertyProperty, value);
            }
        }

        public ObservableCollection<ODataWorkspace> Workspaces { get; set; }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("CurrentModel", typeof(EDMModel), typeof(MetadataViewModel), new PropertyMetadata(null, null));

    }
}
