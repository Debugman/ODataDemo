﻿/*********************************************************

    Copyright (c) Microsoft. All rights reserved.
    This code is licensed under the Microsoft Public License.
    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

*********************************************************/
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SL_OData_Explorer
{
    public class QueryBarViewModel : INotifyPropertyChanged
    {
        private string _currentURI;
        public Func<string, bool> IsValid { get; set; }

        public bool IsValidUri
        {
            get
            {
                if (IsValid != null)
                {
                    return IsValid(CurrentURI);
                }
                return true;
            }
        }
        public string CurrentURI
        {
            get
            {
                if (!String.IsNullOrEmpty(_currentURI))
                {

                    return _currentURI;
                }
                else
                {
                    return String.Empty;
                }
            }
            set
            {
                if (!IsValid(value))
                {
                    throw new ValidationException("Invalid Uri,Please select a Collection from the \"Collections\" list");
                }
                _currentURI = value;
                RaisePropertyChanged("CurrentURI");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
