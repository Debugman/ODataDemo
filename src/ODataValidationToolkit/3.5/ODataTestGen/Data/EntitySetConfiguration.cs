﻿//*********************************************************

//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

//*********************************************************

///<summary>
///Class Name: ODataTestGen.Data.EntitySetConfiguration
///This class stores the the choices on what kind of operations could be performed to an entity set when generating the tests.
///</summary>


namespace ODataTestGen.Data
{
    using System.ComponentModel;
    using System.Linq;

    public class EntitySetConfiguration : INotifyPropertyChanged
    {
        private bool canAppend;
        private bool canDelete;
        private bool canGet;
        private bool canPut;
        private string name;
        private bool isEnabled;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the name of the EntitySet.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set
            {
                this.name = value;
                this.RaiseChanged("Name");
            }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set
            {
                this.GetTest = value;
                this.PutTest = value;
                this.DeleteTest = value;
                this.PostTest = value;
            }
        }

        public bool GetTest
        {
            get { return this.canGet; }
            set
            {
                this.canGet = value;
                this.RaiseChanged("GetTest");
                this.RecalculateIsEnabled();
            }
        }

        public bool PostTest
        {
            get { return this.canAppend; }
            set
            {
                this.canAppend = value;
                this.RaiseChanged("PostTest");
                this.RecalculateIsEnabled();
            }
        }

        public bool PutTest
        {
            get { return this.canPut; }
            set
            {
                this.canPut = value;
                this.RaiseChanged("PutTest");
                this.RecalculateIsEnabled();
            }
        }

        public bool DeleteTest
        {
            get { return this.canDelete; }
            set
            {
                this.canDelete = value;
                this.RaiseChanged("DeleteTest");
                this.RecalculateIsEnabled();
            }
        }

        private void RecalculateIsEnabled()
        {
            bool[] values = new[] { this.canGet, this.canPut, this.canAppend, this.canDelete };
            bool newEnabled = false;

            if (values.Any(c => c))
            {
                newEnabled = true;
            }

            if (this.isEnabled != newEnabled)
            {
                this.isEnabled = newEnabled;
                this.RaiseChanged("IsEnabled");
            }

            
        }

        private void RaiseChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}