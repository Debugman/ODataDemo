﻿//*********************************************************

//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

//*********************************************************


namespace ODataTestGen.Data
{
    public class TestTemplateConfiguration
    {
        public string testName { get; set; }
        public string fullPath { get; set; }
        public string relatedPath { get; set; }

        public TestTemplateConfiguration(string testName, string fullPath, string relatedPath)
        {
            this.testName = testName;
            this.fullPath = fullPath;
            this.relatedPath = relatedPath;
        }
        
    }
}
