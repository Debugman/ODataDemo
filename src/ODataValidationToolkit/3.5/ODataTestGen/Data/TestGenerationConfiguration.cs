﻿//*********************************************************

//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

//*********************************************************

///<summary>
///Class Name: ODataTestGen.Data.EntitySetConfiguration
///This class stores the the choices on what kind of operations could be performed to an entity set when generating the tests.
///</summary>

namespace ODataTestGen.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.Metadata.Edm;
    using System.IO;
    using System.Xml.Linq;

    /// <summary>
    /// Test generation configuration
    /// </summary>
    public class TestGenerationConfiguration
    {
        public TestGenerationConfiguration()
        {
            this.Entities = new List<EntitySetConfiguration>();
            this.Tests = new List<TestTemplateConfiguration>();
            string dir = Directory.GetCurrentDirectory();
            while (!Directory.Exists(Path.Combine(dir, "TestTemplates")))
            {
                DirectoryInfo info = Directory.GetParent(dir);
                if (info == null)
                {
                    this.startingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\");
                    return;
                }
                dir = info.FullName;
            }
            this.startingDirectory = dir;            
        }

        public EdmItemCollection Metadata { get; set; }

        /// <summary>
        /// Gets or sets the base URI of the data service.
        /// </summary>
        public Uri BaseUri { get; set; }

        public List<EntitySetConfiguration> Entities { get; private set; }
        public List<TestTemplateConfiguration> Tests { get; set; }
        public XElement configurationFile { get; set; }
        public string startingDirectory { get; set; }
        public string admxLocation { get; set; }
        public string defaultServiceRoot { get; set; }
        public XElement edmxElement { get; set; }
    }
}