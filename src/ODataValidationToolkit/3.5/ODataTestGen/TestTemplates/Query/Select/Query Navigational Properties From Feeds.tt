<#
	//*********************************************************

	//    Copyright (c) Microsoft. All rights reserved.
	//    This code is licensed under the Microsoft Public License.
	//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
	//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
	//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
	//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

	//*********************************************************
#>
<#@ template debug="true" hostspecific="true" language="C#v3.5" #>
<#@ output extension=".cs" #>
<#@ include file="..\..\Common2.ttinclude" #>
<#@ include file="..\..\UriAnalyzer.ttinclude" #>
<#@ include file="..\..\AtomPayloadAnalyzer.ttinclude" #>
<#
	string testClassName = "QueryAllNavigationPropertiesFromFeeds";
	string description = "query all navigation properties from feeds";
	WriteHeader(testClassName);
#>

<#		
	XNamespace edmxNamespace = edmx.GetNamespaceOfPrefix("edmx");
	XElement dataServicesElement = edmx.Element(edmxNamespace + "DataServices");
	XElement[] schemas = dataServicesElement.Elements().Where(c => c.Name.LocalName == "Schema").ToArray();
	XElement schema = schemas[0];
	XNamespace schemaNamespace = schema.GetDefaultNamespace(); 
	
	XElement entityConfigurations = admxRoot.Element(xmlNamespace + "EntityConfigurations");
	foreach (XElement entity in entityConfigurations.Elements(xmlNamespace + "EntitySet"))
    {	
		if(entity.Attribute("IsEnabled").Value.Equals("false") || entity.Attribute("GetTest").Value.Equals("false"))
			continue;
			
		string entityName = entity.Attribute("Name").Value;
		string entitySetUri = GetAbsoluteUri(serviceRoot, entityName);		
		XElement[] naviProps = getEntityTypeElement(edmx, entityName).Elements().Where(c=>c.Name.LocalName == "NavigationProperty").ToArray();
		foreach (XElement naviProperty in naviProps)
		{
			string propertyName = naviProperty.Attribute("Name").Value;		
#>
/// <summary>
/// Query navigation property <#= propertyName #> From Feed <#= entityName #>
/// </summary>			
[TestMethod]
public void Query<#= propertyName #>FromFeed<#= entityName #>()
<#
			WriteLine("{");
			PushIndent("    ");		
			string type = "";
			UriResultType expectedResult = getResultTypeString(edmx, entityName + "/" + propertyName, ref type);
#>
string strQuery = "<#= entitySetUri #>/<#= propertyName #>";
HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strQuery);
HttpWebResponse response = null;
try
{
	response = (HttpWebResponse)request.GetResponse();
}
catch(System.Net.WebException ex)
{
	Assert.AreEqual(HttpStatusCode.BadRequest, ((HttpWebResponse)ex.Response).StatusCode);	
}
<#
			PopIndent();
			WriteLine("}");
			WriteLine("");
			WriteLine("");
		}
	}
#>
<#
	WriteFooter();
#>
