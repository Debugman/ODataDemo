﻿//*********************************************************

//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

//*********************************************************

using System;
using System.Windows;
using System.Windows.Controls;
using ODataTestGen.OData;

namespace ODataTestGen.Wizard
{
    using System.IO;
    using System.Windows.Forms;
    using System.Xml.Linq;
    using ODataTestGen.Data;

    /// <summary>
    /// Interaction logic for SelectEntitySets.xaml
    /// </summary>
    public partial class SelectEntitySets : Page
    {
        public SelectEntitySets()
        {
            InitializeComponent();
        }

        public TestGenerationConfiguration Configuration { get; set; }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            icEntitySetConfiguration.ItemsSource = this.Configuration.Entities;
            disableModifications();
            this.checkBoxDisallowAllModifications.IsChecked = true;
        }

        private void disableModifications()
        {
            foreach (EntitySetConfiguration cfg in this.Configuration.Entities)
                cfg.PutTest = cfg.PostTest = cfg.DeleteTest = false;
        }

        private void checkBoxDisallowAllModifications_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var config in this.Configuration.Entities)
            {
                config.PutTest = false;
                config.PostTest = false;
                config.DeleteTest = false;
            }
        }

        private void checkBoxDisallowAllModifications_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var config in this.Configuration.Entities)
            {
                config.PutTest = true;
                config.PostTest = true;
                config.DeleteTest = true;
            }
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new SelectTestTemplates(this.Configuration));
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = this.Configuration.startingDirectory;
            dlg.Multiselect = false;
            dlg.Filter = "Annotated Edmx File(*.admx)|*.admx|All Files(*.*)|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.filePath.Text = dlg.FileName;
            }
        }

        private EntitySetConfiguration getEntityConfiguration(string name)
        {
            foreach (EntitySetConfiguration cfg in this.Configuration.Entities)
            {
                if (cfg.Name.Equals(name))
                    return cfg;
            }
            return null;

        }

        /// <summary>
        /// The function loads a previous test configuration file (Annotated EDMX File), and reset the UI to reflect the user's choices.
        /// The file stores which entity set the user wants to test and what kind of tests are going to be generated.
        /// </summary>
        private void buttonLoad_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(this.filePath.Text))
            {
                MessageBox.Show("The specified file doesn't exist");
                return;
            }
            try
            {
                this.Configuration.configurationFile = XElement.Load(this.filePath.Text);
                XElement entityConfigurations = this.Configuration.configurationFile.Element(ODataConstants.EdmxNamespace + "EntityConfigurations");

                if (this.Configuration.defaultServiceRoot == null)
                {
                    XElement defaultUri = this.Configuration.configurationFile.Element(ODataConstants.EdmxNamespace + "DefaultServiceUri");
                    this.Configuration.defaultServiceRoot = defaultUri.Value;
                }

                foreach (XElement entity in entityConfigurations.Elements(ODataConstants.EdmxNamespace + "EntitySet"))
                {
                    EntitySetConfiguration cfg = getEntityConfiguration(entity.FirstAttribute.Value);
                    if (cfg == null)
                        continue;

                    if ((string)entity.Attribute("IsEnabled") == "false")
                        cfg.IsEnabled = false;
                    else
                        cfg.IsEnabled = true;
                    if ((string)entity.Attribute("GetTest") == "false")
                        cfg.GetTest = false;
                    else
                        cfg.GetTest = true;
                    if ((string)entity.Attribute("PostTest") == "false")
                        cfg.PostTest = false;
                    else
                        cfg.PostTest = true;
                    if ((string)entity.Attribute("PutTest") == "false")
                        cfg.PutTest = false;
                    else
                        cfg.PutTest = true;
                    if ((string)entity.Attribute("DeleteTest") == "false")
                        cfg.DeleteTest = false;
                    else
                        cfg.DeleteTest = true;
                }
                this.Configuration.admxLocation = this.filePath.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Check the format of file.\n\n" + ex);
                return;
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            ((Window)this.Parent).Close();
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            foreach (EntitySetConfiguration cfg in this.Configuration.Entities)
            {
                cfg.IsEnabled = cfg.GetTest = cfg.PostTest = cfg.PutTest = cfg.DeleteTest = false;
            }
        }


    }
}