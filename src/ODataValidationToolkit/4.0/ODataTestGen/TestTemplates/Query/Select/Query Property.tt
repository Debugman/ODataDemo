<#
	//*********************************************************

	//    Copyright (c) Microsoft. All rights reserved.
	//    This code is licensed under the Microsoft Public License.
	//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
	//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
	//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
	//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

	//*********************************************************
#>
<#@ template debug="true" hostspecific="true" language="C#v3.5" #>
<#@ output extension=".cs" #>
<#@ include file="..\..\Common2.ttinclude" #>
<#@ include file="..\..\UriAnalyzer.ttinclude" #>
<#@ include file="..\..\AtomPayloadAnalyzer.ttinclude" #>
<#
	string testClassName = "QueryProperty";
	string description = "Query the second property of first entry";
	WriteHeader(testClassName);
#>
		
<#		
	XNamespace edmxNamespace = edmx.GetNamespaceOfPrefix("edmx");
	XElement dataServicesElement = edmx.Element(edmxNamespace + "DataServices");
	XElement[] schemas = dataServicesElement.Elements().Where(c => c.Name.LocalName == "Schema").ToArray();
	XElement schema = schemas[0];
	XNamespace schemaNamespace = schema.GetDefaultNamespace(); 
	
	
	XElement entityConfigurations = admxRoot.Element(xmlNamespace + "EntityConfigurations");
	foreach (XElement entity in entityConfigurations.Elements(xmlNamespace + "EntitySet"))
    {	
		if(entity.Attribute("IsEnabled").Value.Equals("false") || entity.Attribute("GetTest").Value.Equals("false"))
			continue;
			
		string entityName = entity.Attribute("Name").Value;	
		string entitySetUri = GetAbsoluteUri(serviceRoot, entityName);
		string entryPath = getEntryPath(entitySetUri, 1);
		string entryKey = "(Failed to get entry key)";
		if(entryPath != null)
			entryKey = entryPath.Substring(entryPath.LastIndexOf('('));

		XElement[] properties = getEntityTypeElement(edmx, entityName).Elements().Where(c=>c.Name.LocalName == "Property").ToArray();
		if(properties.Length < 2)
			continue;
		
		string propertyName = properties[1].Attribute("Name").Value;
#>
/// <summary>
/// Query the second property from <#= entityName #>
/// </summary>
[TestMethod]
public void Query<#= propertyName #>From<#= entityName #>()
<#
		WriteLine("{");
		PushIndent("    ");	
		if(entryPath == null)
		{
			getNoEntry();
			entryPath = entitySetUri + "(Failed to get entry key)";
#>
throw new InvalidOperationException("Test Generator cannot find the key of first entry");
// Please specify a valid key, remove the exception and uncomment the validation codes.
/*
<#
		}	
#>
string strQuery = "<#= entryPath #>/<#= propertyName #>";
HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strQuery);
HttpWebResponse response = (HttpWebResponse)request.GetResponse();
Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
using(XmlReader xrResponse = XmlReader.Create(response.GetResponseStream()))
{
<#
	PushIndent("    ");	
#>
XElement payload = XElement.Load(xrResponse);
<#
	string type = "";
	UriResultType expectedResult = getResultTypeString(edmx, entityName + entryKey + "/" + propertyName, ref type);
#>
UriResultType expectedType = UriResultType.<#= expectedResult #>;
UriResultType actualType = PayloadAnalyzer.getResultType(payload);
Assert.AreEqual(expectedType, actualType);
<#
		PopIndent();
		WriteLine("}");
		WriteLine("response.Close();");
		if(entryKey == "(Failed to get entry key)")
			WriteLine("*/");
		PopIndent();
		WriteLine("}");
		WriteLine("");
		WriteLine("");
	}
#>
<#
	WriteFooter();
#>
