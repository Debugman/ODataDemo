Instructions on templates in this folder.

Project.tt: It generates the '*.csproj' file. 
PayloadAnalyzer.tt: It generates PayloadAnalyzer.cs which is used to verify atom payload.
UriAnalyzer.tt: It generates UriAnalzyer.cs which is used to analyze uri. 

These files can be changed to adapt a different language other than c#.
