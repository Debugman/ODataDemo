﻿//*********************************************************

//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

//*********************************************************

namespace ODataTestGen.OData
{
    using System.Xml.Linq;

    internal static class ODataConstants
    {
        public static readonly XNamespace EdmxNamespace="http://schemas.microsoft.com/ado/2007/06/edmx";
        public static readonly XNamespace awNamespace = "http://schemas.microsoft.com/ado/2006/04/edm/ssdl";
    }
}