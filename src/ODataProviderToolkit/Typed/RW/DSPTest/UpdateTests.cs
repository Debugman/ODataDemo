﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************



namespace DSPTest
{
    using System;
    using System.Data.Services.Client;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class UpdateTests
    {
        private static TestService service;
        private DataServiceContext ctx;

        [ClassInitialize()]
        public static void ClassInitialize(TestContext testContext)
        {
            service = new TestService(typeof(ProductsService));
        }

        [ClassCleanup()]
        public static void ClassCleanup()
        {
            if (service != null)
            {
                service.Dispose();
                service = null;
            }
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            RecreateContext();
            ProductsService.ResetDataContext();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            ProductsService.ResetDataContext();
        }

        private void RecreateContext()
        {
            this.ctx = new DataServiceContext(service.ServiceUri);
        }

        [TestMethod()]
        public void Post()
        {
            ClientProduct product = new ClientProduct()
            {
                ID = 101,
                Name = "Apple",
                Description = "Red apple",
                ReleaseDate = new DateTime(2009, 9, 13)
            };

            this.ctx.AddObject("Products", product);
            this.ctx.SaveChanges();

            // Recreate context to make sure we get fresh information
            RecreateContext();

            // Ask for the entity we just added
            product = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 101).First();
            Assert.IsNotNull(product, "No such product was found.");
            Assert.AreEqual("Apple", product.Name, "The new product doesn't have correct name.");
        }

        [TestMethod()]
        public void Update()
        {
            Update_Inner(SaveChangesOptions.None);
        }

        [TestMethod()]
        public void UpdateReplace()
        {
            Update_Inner(SaveChangesOptions.ReplaceOnUpdate);
        }

        private void Update_Inner(SaveChangesOptions saveChangesOptions)
        {
            ClientProduct bread = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 0).First();
            int originalRating = bread.Rating;
            int newRating = originalRating + 1;
            bread.Rating = newRating;
            ctx.UpdateObject(bread);
            ctx.SaveChanges(saveChangesOptions);

            // Recreate context to make sure we get fresh information
            RecreateContext();

            // Ask for the bread again
            bread = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 0).First();
            Assert.AreEqual(newRating, bread.Rating, "The Rating value was not updated.");
        }

        [TestMethod()]
        public void Delete()
        {
            ClientProduct bread = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 0).First();
            ctx.DeleteObject(bread);
            ctx.SaveChanges();

            // Recreate context to make sure we get fresh information
            RecreateContext();

            // Ask for the bread again
            ctx.IgnoreResourceNotFoundException = true; // So that we don't get an exception
            bread = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 0).FirstOrDefault();
            Assert.IsNull(bread, "No bread should be found after it was deleted.");
        }

        [TestMethod()]
        public void MultipleUpdates()
        {
            MultipleUpdates_Inner(SaveChangesOptions.None);
        }

        [TestMethod()]
        public void MultipleUpdatesBatch()
        {
            MultipleUpdates_Inner(SaveChangesOptions.Batch);
        }

        [TestMethod()]
        public void MultipleUpdatesReplaceAndBatch()
        {
            MultipleUpdates_Inner(SaveChangesOptions.ReplaceOnUpdate | SaveChangesOptions.Batch);
        }

        private void MultipleUpdates_Inner(SaveChangesOptions saveChangesOptions)
        {
            ClientProduct bread = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 0).First();
            bread.Rating++;
            int newRating = bread.Rating;
            ctx.UpdateObject(bread);

            ClientProduct milk = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 1).First();
            ctx.DeleteObject(milk);

            ClientProduct apple = new ClientProduct()
            {
                ID = 101,
                Name = "Apple",
                Description = "Red apple",
                ReleaseDate = new DateTime(2009, 9, 13)
            };
            ctx.AddObject("Products", apple);

            ctx.SaveChanges(saveChangesOptions);

            // Recreate context to make sure we get fresh information
            RecreateContext();

            // Ask for the bread again
            ctx.IgnoreResourceNotFoundException = true; // So that we don't get an exception on deleted things
            bread = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 0).First();
            Assert.AreEqual(newRating, bread.Rating, "The rating was not correctly updated.");
            milk = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 1).FirstOrDefault();
            Assert.IsNull(milk, "The milk product was not correctly deleted.");
            apple = this.ctx.CreateQuery<ClientProduct>("Products").Where(p => p.ID == 101).First();
            Assert.IsNotNull(apple, "The apple product was not correctly added.");
            Assert.AreEqual("Apple", apple.Name, "The added object doesn't have correct property values.");
        }
    }
}