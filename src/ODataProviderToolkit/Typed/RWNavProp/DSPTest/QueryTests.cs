﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace DSPTest
{
    using System;
    using System.Collections.Generic;
    using System.Data.Services.Client;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class QueryTests
    {
        private static TestService service;
        private DataServiceContext ctx;

        [ClassInitialize()]
        public static void ClassInitialize(TestContext testContext)
        {
            service = new TestService(typeof(ProductsService));
        }

        [ClassCleanup()]
        public static void ClassCleanup()
        {
            if (service != null)
            {
                service.Dispose();
                service = null;
            }
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            this.ctx = new DataServiceContext(service.ServiceUri);
        }

        [TestMethod]
        public void Metadata()
        {
            // Verify that we can get the metadata (as the server performs lot of verifications upon that request)
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.ctx.GetMetadataUri());
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "The $metadata didn't return success.");
        }

        [TestMethod]
        public void AllEntities()
        {
            // Intentionally using the server side type so that we can check that the Price property is not filled
            var q = this.ctx.CreateQuery<Product>("Products").ToList();
            Assert.AreEqual(3, q.Count, "The service returned unexpected number of results.");
            Assert.AreEqual(2, q[2].ID, "The ID is not correctly filled.");
            Assert.AreEqual("Milk", q[1].Name, "The Name is not correctly filled.");
            Assert.AreEqual(0, q[1].Price, "Price should not have been included but it got a value.");
            Assert.AreEqual(12, q[0].Quantity.Value, "Unexpected quantity value.");
        }

        [TestMethod]
        public void Filters()
        {
            VerifyEntityCount<ClientProduct>("/Products?$filter=ID eq 1", 1);
            VerifyEntityCount<ClientProduct>("/Products?$filter=ID ge 0", 3);
            VerifyEntityCount<ClientProduct>("/Products?$filter=length(Name) eq 4", 2);
            VerifyEntityCount<ClientProduct>("/Products?$filter=Rating gt 3", 2);
            VerifyEntityCount<ClientProduct>("/Products?$filter=false", 0);
            VerifyEntityCount<ClientProduct>("/Products?$filter=(ID ge 1) and (length(Name) eq 4)", 2);
        }

        [TestMethod]
        public void Projections()
        {
            VerifySelectedProperties<ClientProduct>("/Products?$select=ID&$filter=ID gt 0", "ID");
            VerifySelectedProperties<ClientProduct>("/Products?$select=Name", "Name");
            VerifySelectedProperties<ClientProduct>("/Products?$select=Quantity", "Quantity");
            VerifySelectedProperties<ClientProduct>("/Products?$select=Name,Description,Rating", "Name", "Description", "Rating");
            VerifySelectedProperties<ClientProduct>("/Products?$select=*&$filter=ID eq 2", "ID", "Name", "Description", "ReleaseDate", "DiscontinueDate", "Rating", "Quantity");
        }

        [TestMethod]
        public void ResourceReferenceProperty()
        {
            List<ClientCategory> categories = ctx.CreateQuery<ClientCategory>("Categories").ToList();
            var category = RunSingleResultQuery<ClientCategory>("/Products(0)/Category");
            Assert.AreEqual(categories[0], category, "The product ID 0 should be in the first category.");
            category = RunSingleResultQuery<ClientCategory>("/Products(1)/Category");
            Assert.AreEqual(categories[1], category, "The product ID 1 should be in the second category.");
            category = RunSingleResultQuery<ClientCategory>("/Products(2)/Category");
            Assert.AreEqual(categories[1], category, "The product ID 2 should be in the second category.");
        }

        [TestMethod]
        public void ResourceSetReferenceProperty()
        {
            List<ClientProduct> products = ctx.CreateQuery<ClientProduct>("Products").ToList();
            var relatedProducts = RunQuery<ClientProduct>("/Categories(0)/Products").ToList();
            Assert.AreEqual(1, relatedProducts.Count, "Category 0 should have just one product.");
            Assert.IsTrue(relatedProducts.Contains(products[0]), "The category 0 should have product 0 in it.");
            relatedProducts = RunQuery<ClientProduct>("/Categories(1)/Products").ToList();
            Assert.AreEqual(2, relatedProducts.Count, "Category 1 should have two products.");
            Assert.IsTrue(relatedProducts.Contains(products[1]), "The category 1 should have product 1 in it.");
            Assert.IsTrue(relatedProducts.Contains(products[2]), "The category 1 should have product 2 in it.");
            relatedProducts = RunQuery<ClientProduct>("/Categories(2)/Products").ToList();
            Assert.AreEqual(0, relatedProducts.Count, "Category 2 should have no products.");
        }

        private void VerifyEntityCount<TElement>(string queryUri, int expectedEntityCount)
        {
            var q = RunQuery<TElement>(queryUri);
            Assert.AreEqual(expectedEntityCount, q.Count(), "Query '" + queryUri + "' didn't return expected number of entities.");
        }

        private void VerifySelectedProperties<TElement>(string queryUri, params string[] selectedProperties)
        {
            MergeOption mergeOptions = this.ctx.MergeOption;
            // No tracking as we need new instances returned each time we query (so that properties which were not in the response get their default values)
            this.ctx.MergeOption = MergeOption.NoTracking;
            try
            {

                var q = RunQuery<TElement>(queryUri);

                foreach (TElement item in q)
                {
                    foreach (var property in typeof(TElement).GetProperties())
                    {
                        object propertyValue = property.GetValue(item, null);
                        object defaultValue = GetDefaultValueForType(property.PropertyType);

                        if (selectedProperties.Contains(property.Name))
                        {
                            Assert.AreNotEqual(defaultValue, propertyValue, "Property '" + property.Name + "' has default value even though it was selected.");
                        }
                        else
                        {
                            Assert.AreEqual(defaultValue, propertyValue, "Property '" + property.Name + "' doesn't have default value even though it was not selected.");
                        }
                    }
                }
            }
            finally
            {
                this.ctx.MergeOption = mergeOptions;
            }
        }

        private static MethodInfo GetDefaultValueForTypeInnerMethod = typeof(QueryTests).GetMethod("GetDefaultValueForTypeInner", BindingFlags.NonPublic | BindingFlags.Static);

        private static object GetDefaultValueForType(Type t)
        {
            return GetDefaultValueForTypeInnerMethod.MakeGenericMethod(t).Invoke(null, null);
        }

        private static object GetDefaultValueForTypeInner<T>()
        {
            return default(T);
        }

        private IEnumerable<TElement> RunQuery<TElement>(string queryUri)
        {
            return this.ctx.Execute<TElement>(new Uri(queryUri, UriKind.Relative));
        }

        private TElement RunSingleResultQuery<TElement>(string queryUri)
        {
            return RunQuery<TElement>(queryUri).AsEnumerable().Single();
        }
    }
}
