﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace DataServiceProvider
{
    using System;
    using System.Data.Services;
    using System.Data.Services.Providers;
    using System.Web;

    /// <summary>Data service implementation which can defined metadata for the service and stores the data as property bags.</summary>
    /// <typeparam name="T">The type of the context to use. This must derive from the <see cref="DSPContext"/> class.</typeparam>
    public abstract class DSPDataService<T> : DataService<T>, IServiceProvider where T : DSPContext
    {
        /// <summary>The metadata definition. This also provides the <see cref="IDataServiceMetadataProvider"/> implementation.</summary>
        private DSPMetadata metadata;
        private DSPMetadataModelExtension metadataExtension;

        /// <summary>The resource query provider implementation for the service. Implements <see cref="IDataServiceQueryProvider"/>.</summary>
        private DSPResourceQueryProvider resourceQueryProvider;
        private DSPResourceQueryProviderModelExtension resourceQueryProviderExtension;

        /// <summary>Constructor</summary>
        public DSPDataService()
        {
        }

        /// <summary>Abstract method which a derived class implements to create the metadata for the service.</summary>
        /// <returns>The metadata definition for the service. Note that this is called only once per the service lifetime.</returns>
        protected abstract DSPMetadata CreateDSPMetadata();

        /// <summary>Returns the metadata definition for the service. It will create it if no metadata is available yet.</summary>
        protected DSPMetadata Metadata
        {
            get
            {
                if (this.metadata == null)
                {
                    this.metadata = CreateDSPMetadata();
                    this.metadata.SetReadOnly();
                    this.metadataExtension = new DSPMetadataModelExtension(this.metadata);
                    if (HttpContext.Current != null)
                    {
                        string[] extensions = HttpContext.Current.Request.QueryString.GetValues("modelextension");
                        if (extensions != null)
                        {
                            foreach (string me in extensions)
                            {
                                this.metadataExtension.AddExtension(me);
                            }
                        }
                    }
                    this.metadataExtension.SetReadOnly();
                    this.resourceQueryProvider = new DSPResourceQueryProvider();
                    this.resourceQueryProviderExtension = new DSPResourceQueryProviderModelExtension(this.metadataExtension, this.resourceQueryProvider);
                }

                return this.metadata;
            }
        }

        #region IServiceProvider Members

        /// <summary>Returns service implementation.</summary>
        /// <param name="serviceType">The type of the service requested.</param>
        /// <returns>Implementation of such service or null.</returns>
        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(IDataServiceMetadataProvider))
            {
                DSPMetadata metadata = this.Metadata;
                return this.metadataExtension;
            }
            else if (serviceType == typeof(IDataServiceQueryProvider))
            {
                return this.resourceQueryProviderExtension;
            }
            else if (serviceType == typeof(IDataServiceUpdateProvider))
            {
                return new DSPUpdateProvider(this.CurrentDataSource, this.Metadata);
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
