﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace AstoriaOverAstoriaTests
{
    using System.Data.Services;
    using System;

    public class TestDataService
    {
        public static Action<DataServiceConfiguration> InitializeService;
        public static bool ForceVerboseErrors = true;

        internal static void InnerInitializeService(DataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.All);
            config.DataServiceBehavior.AcceptCountRequests = true;
            config.DataServiceBehavior.AcceptProjectionRequests = true;
            config.DataServiceBehavior.MaxProtocolVersion = System.Data.Services.Common.DataServiceProtocolVersion.V2;

            config.UseVerboseErrors = ForceVerboseErrors;

            if (InitializeService != null)
            {
                InitializeService(config);
            }
        }
    }

    public class TestDataService<T> : DataService<T>
    {
        public static void InitializeService(DataServiceConfiguration config)
        {
            TestDataService.InnerInitializeService(config);
        }
    }
}
