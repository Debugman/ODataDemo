﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace AstoriaOverAstoriaTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using customersModel = AstoriaOverAstoriaTests.CustomersModel;

    [TestClass]
    public class SimpleTests
    {
        [TestMethod]
        public void Plain()
        {
            VerifyGetResponseOnCustom("/Customers");
            VerifyGetResponseOnCustom("/Animals");
        }

        [TestMethod]
        public void ResourceNavigation()
        {
            VerifyGetResponseOnCustom("/Customers(0)");
            VerifyGetResponseOnCustom("/Customers(-1)");
        }

        [TestMethod]
        public void PropertyAccess()
        {
            for (int customerId = 0; customerId < 4; customerId++)
            {
                string customer = "/Customers(" + customerId.ToString() + ")";
                VerifyGetResponseOnCustom(customer + "/Name");
                VerifyGetResponseOnCustom(customer + "/ID");
                VerifyGetResponseOnCustom(customer + "/NullableBoolean");
                if (customerId > 0 && customerId < 3)
                {
                    VerifyGetResponseOnCustom(customer + "/BestFriend/Name");
                    VerifyGetResponseOnCustom(customer + "/BestFriend/ID");
                    VerifyGetResponseOnCustom(customer + "/BestFriend/NullableBoolean");
                    VerifyGetResponseOnCustom(customer + "/Friends(0)/Name");
                    VerifyGetResponseOnCustom(customer + "/Friends(0)/ID");
                    VerifyGetResponseOnCustom(customer + "/Friends(0)/NullableBoolean");
                }
            }
        }

        [TestMethod]
        public void PropertyNavigation()
        {
            VerifyGetResponseOnCustom("/Customers(0)/Friends");    // empty
            VerifyGetResponseOnCustom("/Customers(0)/BestFriend"); // null
            VerifyGetResponseOnCustom("/Customers(1)/BestFriend"); // non-null
            VerifyGetResponseOnCustom("/Customers(0)/Friends");    // empty
            VerifyGetResponseOnCustom("/Customers(1)/Friends");    // 1 friend
            VerifyGetResponseOnCustom("/Customers(2)/Friends");    // 2 friends
            VerifyGetResponseOnCustom("/Customers(1)/BestFriend/Friends"); // empty
            VerifyGetResponseOnCustom("/Customers(2)/BestFriend/Friends"); // 1 friend
            VerifyGetResponseOnCustom("/Customers(1)/BestFriend/BestFriend"); // null
            VerifyGetResponseOnCustom("/Customers(2)/BestFriend/BestFriend"); // non-null
            VerifyGetResponseOnCustom("/Customers(2)/Friends(0)/BestFriend"); // null
            VerifyGetResponseOnCustom("/Customers(2)/Friends(1)/BestFriend"); // non-null
            VerifyGetResponseOnCustom("/Customers(2)/Friends(0)/Friends"); // empty
            VerifyGetResponseOnCustom("/Customers(2)/Friends(1)/Friends"); // 1 friend
            VerifyGetResponseOnCustom("/Customers(-1)/Friends");

            // BUG - This is a known problem - Client LINQ reports /Customers(0)/BestFriend as well as /Customers(-1)/BestFriend both as empty enumerations
            //   (with IgnoreResourceNotFound set to true, which is needed). To make the first case work we turn such empty enumeration into
            //   a single null, which unfortunately means that we do the same for the latter case as well.
            // This should return 404
            //VerifyResourceNotFoundResponse("/Customers(-1)/BestFriend");
        }

        [TestMethod]
        public void Filters()
        {
            VerifyGetResponseOnCustom("/Customers?$filter=ID eq 1");  // 1 result
            VerifyGetResponseOnCustom("/Customers?$filter=ID eq -1");  // 0 results
            VerifyGetResponseOnCustom("/Customers?$filter=ID gt 0");  // 2 results
            VerifyGetResponseOnCustom("/Customers?$filter=Name eq 'Customer 0'");
            VerifyGetResponseOnCustom("/Customers?$filter=startswith(Name, 'Customer')");
            VerifyGetResponseOnCustom("/Customers?$filter=length(Name) eq 10");
            VerifyGetResponseOnCustom("/Customers?$filter=isof('AstoriaOverAstoriaTests.CustomersModel.Customer')");
            VerifyGetResponseOnCustom("/Customers?$filter=length(cast('AstoriaOverAstoriaTests.CustomersModel.Customer')/Name) ge 10");
            VerifyGetResponseOnCustom("/Customers?$filter=NullableBoolean");
            VerifyGetResponseOnCustom("/Customers?$filter=NullableBoolean ne true");
            VerifyGetResponseOnCustom("/Customers?$filter=NullableBoolean eq true");
            VerifyGetResponseOnCustom("/Customers?$filter=Name eq null");
            VerifyGetResponseOnCustom("/Customers?$filter=ID gt 0 and startswith(Name, 'Customer')");
        }

        [TestMethod]
        public void Projections()
        {
            foreach (string customer in new string[] { "/Customers", "/Customers(0)", "/Customers(2)" })
            {
                VerifyGetResponseOnCustom(customer + "?$select=Name");
                VerifyGetResponseOnCustom(customer + "?$select=ID");
                VerifyGetResponseOnCustom(customer + "?$select=Name,ID");
                VerifyGetResponseOnCustom(customer + "?$select=ID,Name");
                VerifyGetResponseOnCustom(customer + "?$select=BestFriend");
                VerifyGetResponseOnCustom(customer + "?$select=Friends");
                VerifyGetResponseOnCustom(customer + "?$select=*");
                VerifyGetResponseOnCustom(customer + "?$select=NullableBoolean");
            }

            foreach (string bigObject in new string[] { "/BigObjects", "/BigObjects(0)" })
            {
                VerifyGetResponseOnCustom(bigObject + "?$select=ID");
                VerifyGetResponseOnCustom(bigObject + "?$select=*");
                VerifyGetResponseOnCustom(bigObject + "?$select=IntegerProperty0,IntegerProperty1,IntegerProperty2,IntegerProperty3,IntegerProperty4,IntegerProperty5");
                VerifyGetResponseOnCustom(bigObject + "?$select=IntegerProperty0,IntegerProperty1,IntegerProperty2,IntegerProperty3,IntegerProperty4,IntegerProperty5," +
                    "IntegerProperty6,IntegerProperty7,IntegerProperty8,IntegerProperty9");
                VerifyGetResponseOnCustom(bigObject + "?$select=IntegerProperty0,IntegerProperty1,IntegerProperty2,IntegerProperty3,IntegerProperty4,IntegerProperty5," +
                    "IntegerProperty6,IntegerProperty7,IntegerProperty8,IntegerProperty9," +
                    "StringProperty0,StringProperty1,StringProperty2,StringProperty3,StringProperty4,StringProperty5," +
                    "StringProperty6,StringProperty7,StringProperty8,StringProperty9");
            }

            VerifyGetResponseOnCustom("/Customers(-1)?$select=Name");
        }

        [TestMethod]
        public void Expansions()
        {
            foreach (string customer in new string[] { "/Customers", "/Customers(0)", "/Customers(2)" })
            {
                VerifyGetResponseOnCustom(customer + "?$expand=Friends");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/BestFriend");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/BestFriend/BestFriend");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/Friends");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/Friends/BestFriend");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends/Friends");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends/Friends/Friends");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends/Friends/BestFriend");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends/BestFriend");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends/BestFriend/BestFriend");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends/BestFriend,Friends/Friends");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/BestFriend,BestFriend/Friends");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/BestFriend,BestFriend/Friends,Friends");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/BestFriend/Friends,BestFriend/Friends/BestFriend,Friends");
            }
        }

        [TestMethod]
        public void ExpansionsWithProjections()
        {
            foreach (string customer in new string[] { "/Customers", "/Customers(0)", "/Customers(2)" })
            {
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend&$select=Name,BestFriend/Name");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends&$select=*,Friends/Name");
                VerifyGetResponseOnCustom(customer + "?$expand=Friends&$select=Name,Friends/Name");
                VerifyGetResponseOnCustom(customer + "?$expand=BestFriend/Friends&$select=BestFriend/Friends/Name");
            }
        }

        [TestMethod]
        public void Inheritance()
        {
            VerifyGetResponseOnCustom("/Animals");
            VerifyGetResponseOnCustom("/Animals(2)");
            VerifyGetResponseOnCustom("/Animals(-1)");
            VerifyGetResponseOnCustom("/Animals?$filter=ID eq 0");
            VerifyGetResponseOnCustom("/Animals?$expand=Related");
            VerifyGetResponseOnCustom("/Animals?$expand=SameKindAnimals");
            VerifyGetResponseOnCustom("/Animals?$expand=Related/Related/Related");
            VerifyGetResponseOnCustom("/Animals?$expand=SameKindAnimals/SameKindAnimals/SameKindAnimals");
            VerifyGetResponseOnCustom("/Animals?$expand=Related/SameKindAnimals,SameKindAnimals/Related");
        }

        [TestMethod]
        public void InheritanceProjections()
        {
            foreach (string animal in new string[] { "/Animals", "/Animals(0)", "/Animals(1)", "/Animals(2)" })
            {
                VerifyGetResponseOnCustom(animal + "?$select=ID");
                VerifyGetResponseOnCustom(animal + "?$select=ID,Related");
                VerifyGetResponseOnCustom(animal + "?$select=*");
                VerifyGetResponseOnCustom(animal + "?$select=Related/ID&$expand=Related");
                VerifyGetResponseOnCustom(animal + "?$select=Related/ID,ID&$expand=Related");
                VerifyGetResponseOnCustom(animal + "?$select=Related/ID,*&$expand=Related");
                // This will fail due to the limitation of the client side projections.
                //    In this case we want the Animal(1) (which is a Cat) in fact twice in the query, once as the Animal(1) and once as Animal(2).Related
                //    but each time with a different set of properties and in fact even different type. This results in the problem where
                //    the original service will return the Animal(2).Related as a fullblown Cat object (including the Color property)
                //    but we can't emulate that (or better to say client LINQ doesn't support projecting the same instance as two types).
                //VerifyGetResponseOnCustom(animal + "?$select=Related/*,ID&$expand=Related");
                VerifyGetResponseOnCustom(animal + "?$select=SameKindAnimals/ID,ID&$expand=SameKindAnimals");
                VerifyGetResponseOnCustom(animal + "?$select=SameKindAnimals/ID,*&$expand=SameKindAnimals");
                // Same problem as with the query above - not supported
                //VerifyGetResponseOnCustom(animal + "?$select=SameKindAnimals/*,ID&$expand=SameKindAnimals");
                VerifyGetResponseOnCustom(animal + "?$select=SameKindAnimals/ID,Related/ID,ID&$expand=SameKindAnimals,Related");
                VerifyGetResponseOnCustom(animal + "?$select=SameKindAnimals/SameKindAnimals/Related/ID,ID&$expand=SameKindAnimals/SameKindAnimals/Related,Related");
            }
        }

        [TestMethod]
        public void OrderBy()
        {
            VerifyGetResponseOnCustom("/Customers?$orderby=ID");
            VerifyGetResponseOnCustom("/Customers?$orderby=Name");
            VerifyGetResponseOnCustom("/Customers?$orderby=NullableBoolean");
            VerifyGetResponseOnCustom("/Customers?$orderby=BestFriend/ID");
            VerifyGetResponseOnCustom("/Customers?$orderby=BestFriend/Name");
            VerifyGetResponseOnCustom("/Customers?$orderby=BestFriend/NullableBoolean");
            VerifyGetResponseOnCustom("/Customers?$orderby=length(Name) add ID");
            VerifyGetResponseOnCustom("/Customers?$orderby=length(cast('AstoriaOverAstoriaTests.CustomersModel.Customer')/Name) add ID");
            VerifyGetResponseOnCustom("/Customers?$orderby=isof('AstoriaOverAstoriaTests.CustomersModel.Customer')");
            VerifyGetResponseOnCustom("/Customers?$orderby=ID,Name,NullableBoolean");
            VerifyGetResponseOnCustom("/Customers?$orderby=length(cast('AstoriaOverAstoriaTests.CustomersModel.Customer')/Name) add ID,length(cast('AstoriaOverAstoriaTests.CustomersModel.Customer')/Name) sub ID");

            // With expansions and projections
            VerifyGetResponseOnCustom("/Customers?" +
                "$orderby=length(cast('AstoriaOverAstoriaTests.CustomersModel.Customer')/Name) add ID&" +
                "$select=Name,Friends/ID,BestFriend/BestFriend/ID&" +
                "$expand=Friends,BestFriend/BestFriend");
            VerifyGetResponseOnCustom("/Customers?" +
                "$orderby=Name,length(cast('AstoriaOverAstoriaTests.CustomersModel.Customer')/Name) add ID&" +
                "$select=Name,Friends/ID,BestFriend/BestFriend/ID&" +
                "$expand=Friends,BestFriend/BestFriend");
        }

        [TestMethod]
        public void Count()
        {
            foreach (string countOption in new string[] { "/$count", "?$inlinecount=allpages" })
            {
                VerifyGetResponseOnCustom("/Customers" + countOption);
                VerifyGetResponseOnCustom("/Customers(0)/Friends" + countOption);
                VerifyGetResponseOnCustom("/Customers(1)/Friends" + countOption);
                VerifyGetResponseOnCustom("/Customers(2)/Friends" + countOption);
                // This is a known bug in Reflection provider implementation - this query fails with NRE on the server 
                //   when running over Reflection provider with Linq to Objects
                //VerifyGetResponseOnCustom("/Customers(0)/BestFriend/Friends/$count");
                VerifyGetResponseOnCustom("/Customers(1)/BestFriend/Friends" + countOption);
                VerifyGetResponseOnCustom("/Customers(2)/BestFriend/Friends" + countOption);
            }

            VerifyGetResponseOnCustom("/Customers?$select=Name&$inlinecount=allpages");
            VerifyGetResponseOnCustom("/Customers?$select=Name,BestFriend/ID,BestFriend/Friends/Name&$expand=BestFriend,BestFriend/Friends&$inlinecount=allpages");

            VerifyGetResponseOnCustom("/Customers/$count?$filter=Name");
            VerifyGetResponseOnCustom("/Customers/$count?$filter=(length(Name) gt 0) and (ID lt 2)");
        }

        [TestMethod]
        public void Value()
        {
            foreach (string customer in new string[] { "/Customers(0)", "/Customers(1)", "/Customers(2)", "/Customers(3)" })
            {
                VerifyGetResponseOnCustom(customer + "/Name/$value");
                VerifyGetResponseOnCustom(customer + "/ID/$value");
                VerifyGetResponseOnCustom(customer + "/NullableBoolean/$value");
                VerifyGetResponseOnCustom(customer + "/BestFriend/Name/$value");
            }
        }

        private void VerifyGetResponseOnCustom(string uri)
        {
            AoATestUtil.VerifyGetResponse(typeof(customersModel.CustomAoAService), uri);
        }
    }
}
