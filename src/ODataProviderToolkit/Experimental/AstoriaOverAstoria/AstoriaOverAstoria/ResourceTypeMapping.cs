﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace AstoriaOverAstoria
{
    using System;
    using System.Data.Services.Providers;

    /// <summary>Class which represents mapping between the client and server resource type.</summary>
    /// <remarks>Note that the class is actually the client resource type, but it can contain information
    /// about mapping to the server resource type.</remarks>
    internal class ResourceTypeMapping : ResourceType
    {
        /// <summary>Constructor.</summary>
        /// <param name="clientClrType">The underlying client CLR type.</param>
        /// <param name="resourceTypeKind">The kind of the resource type.</param>
        /// <param name="baseType">The base type for this type.</param>
        /// <param name="namespaceName">The namespace name to which the resource type belons.</param>
        /// <param name="name">The name of the resource type (same for client and server).</param>
        /// <param name="isAbstract">If the resource type is abstract.</param>
        public ResourceTypeMapping(
            Type clientClrType, 
            ResourceTypeKind resourceTypeKind, 
            ResourceTypeMapping baseType, 
            string namespaceName,
            string name,
            bool isAbstract)
            : base(clientClrType, resourceTypeKind, baseType, namespaceName, name, isAbstract)
        {
        }
    }
}