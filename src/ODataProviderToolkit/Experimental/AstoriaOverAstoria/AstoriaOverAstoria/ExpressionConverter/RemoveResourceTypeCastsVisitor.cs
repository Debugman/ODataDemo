﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace AstoriaOverAstoria
{
    using System;
    using System.Data.Services.Providers;
    using System.Diagnostics;
    using System.Linq.Expressions;

    /// <summary>Visitor which removes any casts to entity ResourceType using the "as" operator.</summary>
    internal class RemoveResourceTypeCastsVisitor : ResourceAnnotationPreservingExpressionVisitor
    {
        /// <summary>Creates new visitor.</summary>
        /// <param name="annotations">The expression annotations to use.</param>
        private RemoveResourceTypeCastsVisitor(ExpressionResourceAnnotations annotations)
            : base(annotations)
        {
        }

        /// <summary>Process the specified expression to remove any resource type casts from it.</summary>
        /// <param name="expression">The expression to process.</param>
        /// <param name="annotations">The expression resource annotations.</param>
        /// <returns>The expression without the resource type casts.</returns>
        public static Expression Process(Expression expression, ExpressionResourceAnnotations annotations)
        {
            RemoveResourceTypeCastsVisitor visitor = new RemoveResourceTypeCastsVisitor(annotations);
            return visitor.Visit(expression);
        }

        /// <summary>Visits unary expression.</summary>
        /// <param name="u">The unary expression.</param>
        /// <returns>The visited expression.</returns>
        internal override Expression VisitUnary(UnaryExpression u)
        {
            if (u.NodeType == ExpressionType.TypeAs)
            {
                Expression operand = this.Visit(u.Operand);
                ResourceType resourceType = this.Annotations.GetResourceType(operand);
                if (u.Type.IsAssignableFrom(resourceType.InstanceType))
                {
                    return operand;
                }
                else
                {
                    // This cast destroys the resource type information because it converts to unrecognized type.
                    return Expression.TypeAs(operand, u.Type);
                }
            }

            return base.VisitUnary(u);
        }
    }
}