﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace AstoriaOverAstoria
{
    using System;
    using System.Data.Services.Providers;

    /// <summary>Class which represents mapping between the client and server resource set.</summary>
    /// <remarks>Note that the class is actually the client resource set itself, but it may contain more information
    /// regarding mapping that to the server resource set.</remarks>
    internal class ResourceSetMapping : ResourceSet
    {
        /// <summary>Constructor.</summary>
        /// <param name="name">The name of the resource set (same for client and server).</param>
        /// <param name="elementType">The base type of the element in the resource set.</param>
        public ResourceSetMapping(string name, ResourceTypeMapping elementType)
            : base(name, elementType)
        {
        }
    }
}
