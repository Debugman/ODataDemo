﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace AstoriaOverAstoria
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>Implementation of <see cref="IQueryable"/> which turns server generated expressions
    /// into expressions accepted by the client library.</summary>
    /// <typeparam name="T">The type of the query results.</typeparam>
    internal class AstoriaOverAstoriaQuery<T> : IOrderedQueryable<T>
    {
        /// <summary>The query provider for this query.</summary>
        private AstoriaOverAstoriaQueryProvider queryProvider;

        /// <summary>The expression tree for the query.</summary>
        private Expression queryExpression;

        /// <summary>Creates a new query.</summary>
        /// <param name="queryProvider">The query provider for the query.</param>
        /// <param name="queryExpression">The expression tree for the query.</param>
        internal AstoriaOverAstoriaQuery(
            AstoriaOverAstoriaQueryProvider queryProvider, 
            Expression queryExpression)
        {
            this.queryProvider = queryProvider;
            this.queryExpression = queryExpression;
        }

        /// <summary>The type of the query results.</summary>
        Type IQueryable.ElementType
        {
            get { return typeof(T); }
        }

        /// <summary>The expression tree for the query.</summary>
        Expression IQueryable.Expression
        {
            get { return this.queryExpression; }
        }

        /// <summary>The query provider for the query.</summary>
        IQueryProvider IQueryable.Provider
        {
            get { return this.queryProvider; }
        }

        /// <summary>Returns new enumerator to enumerate the results of the query.</summary>
        /// <returns>New enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            ExpressionConversionResult result = this.queryProvider.ConvertExpression(this.queryExpression);

            IQueryable query = this.queryProvider.CreateSourceQuery(result.Expression);
            Trace.WriteLine("Underlying: " + query.ToString());
            if (result.QueryResultsProcessor != null)
            {
                var r = result.QueryResultsProcessor.ProcessResults((IEnumerable)query, this.queryProvider.Context).Cast<object>().ToList();
                return r.Cast<T>().GetEnumerator();
            }
            else
            {
                return (IEnumerator<T>)query.GetEnumerator();
            }
        }

        /// <summary>Returns new enumerator to enumerate the results of the query.</summary>
        /// <returns>New enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)this).GetEnumerator();
        }
    }
}