﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace AstoriaOverAstoria
{
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>Interface used to represent a single query results processor.</summary>
    internal interface IQueryResultsProcessor
    {
        /// <summary>Method which processes a single result of a query before it is handed to the server.</summary>
        /// <param name="result">The result produced by the query (and any result processors) so far.</param>
        /// <param name="context">The context which is executing the query.</param>
        /// <returns>The result of the query after the transformation.</returns>
        object ProcessSingleResult(object result, AstoriaOverAstoriaContext context);

        /// <summary>Method which processes the results of a query before they are handed to the server.</summary>
        /// <param name="results">The results of the query so far.</param>
        /// <param name="context">The context which is executing the query.</param>
        /// <returns>The results of the query after the transformation.</returns>
        IEnumerable ProcessResults(IEnumerable results, AstoriaOverAstoriaContext context);
    }
}
