﻿//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace DSPTest
{
    using System;
    using System.ServiceModel.Web;
    using System.Threading;

    public class TestService : IDisposable
    {
        private WebServiceHost host;
        private Uri serviceUri;
        private static int lastHostId = 1;

        public TestService(Type serviceType)
        {
            for (int i = 0; i < 100; i++)
            {
                int hostId = Interlocked.Increment(ref lastHostId);
                this.serviceUri = new Uri("http://localhost/Temporary_Listen_Addresses/DSPTestService" + hostId.ToString());
                this.host = new WebServiceHost(serviceType, this.serviceUri);
                try
                {
                    this.host.Open();
                    break;
                }
                catch (Exception)
                {
                    this.host.Abort();
                    this.host = null;
                }
            }

            if (this.host == null)
            {
                throw new InvalidOperationException("Could not open a service even after 100 tries.");
            }
        }

        public void Dispose()
        {
            if (this.host != null)
            {
                this.host.Close();
                this.host = null;
            }
        }

        public Uri ServiceUri
        {
            get { return this.serviceUri; }
        }
    }
}
