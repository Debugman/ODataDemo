﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ProductService.Models;

namespace ProductService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ProductService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Rating>("Ratings");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RatingsController : EntitySetController<Rating,int>
    {
        private ProductsContext db = new ProductsContext();

        // GET: odata/Ratings
        [EnableQuery]
        public IQueryable<Rating> GetRatings()
        {
            return db.Ratings;
        }

        // GET: odata/Ratings(5)
        [EnableQuery]
        public SingleResult<Rating> GetRating([FromODataUri] int key)
        {
            return SingleResult.Create(db.Ratings.Where(rating => rating.Id == key));
        }

        // PUT: odata/Ratings(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<Rating> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Rating rating = await db.Ratings.FindAsync(key);
            if (rating == null)
            {
                return NotFound();
            }

            patch.Put(rating);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RatingExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rating);
        }

        // POST: odata/Ratings
        public async Task<IHttpActionResult> Post(Rating rating)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Ratings.Add(rating);
            await db.SaveChangesAsync();

            return Created(rating);
        }

        // PATCH: odata/Ratings(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Rating> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Rating rating = await db.Ratings.FindAsync(key);
            if (rating == null)
            {
                return NotFound();
            }

            patch.Patch(rating);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RatingExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rating);
        }

        // DELETE: odata/Ratings(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            Rating rating = await db.Ratings.FindAsync(key);
            if (rating == null)
            {
                return NotFound();
            }

            db.Ratings.Remove(rating);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RatingExists(int key)
        {
            return db.Ratings.Count(e => e.Id == key) > 0;
        }
    }
}
