﻿using Simple.OData.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODataClientDemo
{
    class Program
    {
        // Get an entire entity set.
        static void ListAllProducts(Default.Container container)
        {
            foreach (var p in container.Products)
            {
                                              
                Console.WriteLine("name:{0}price:{1}Category:{2}Id:{3}", p.Name, p.Price, p.Category,p.Id);
            }
        }

        static void AddProduct(Default.Container container, ProductService.Models.Product product)
        {
            container.AddToProducts(product);
            var serviceResponse = container.SaveChanges();
            foreach (var operationResponse in serviceResponse)
            {
                Console.WriteLine("Response: {0}", operationResponse.StatusCode);
            }
        }
        static void DeleteProduct(Default.Container container,ProductService.Models.Product product)
        {
            container.DeleteObject(product);
            
            var serviceResponse = container.SaveChanges();
            foreach(var operationResponse in serviceResponse)
            {
                Console.WriteLine("Response:{0}", operationResponse.StatusCode);
            }
        } 
        
        static void UpdateProduct(Default.Container container,ProductService.Models.Product product)
        {
            container.UpdateObject(product);
            var serviceResponse = container.SaveChanges();
            foreach(var operationResponse in serviceResponse)
            {
                Console.WriteLine("Resposne:{0}", operationResponse.StatusCode);
            }
        }

        /// <summary>
        /// Simple.OData.Client
        /// https://github.com/object/Simple.OData.Client.git
        /// </summary>
        public static async Task<IEnumerable<ProductService.Models.Product>> TestODataClient() {
            string serviceUri = "http://localhost:50760/";

            var client = new ODataClient(new ODataClientSettings(serviceUri) {
               IgnoreResourceNotFoundException = true,
               OnTrace = (x,y)=>Console.WriteLine(string.Format(x,y))
            });
            //                       
            var annotations = new ODataFeedAnnotations();
            var people = await client
                .For<ProductService.Models.Product>()
                .Key("Id")
                .Expand(x => new { x.Supplier })
                .FindEntriesAsync(annotations);
            return people;
        }

        static void Main(string[] args)
        {


            TestODataClient();




            // TODO: Replace with your local URI.
            string serviceUri = "http://localhost:50760/";
            var container = new Default.Container(new Uri(serviceUri));
            //             var product = new ProductService.Models.Product()
            //             {
            //                 Name = "Yo-yo",
            //                 Category = "Toys",
            //                 Price = 4.95M
            //             };    
            //             AddProduct(container, product);

            //for (int i = 0; i < 100; i++) {
            //    ProductService.Models.Product product = new ProductService.Models.Product();
            //    product.Name = "test" + i.ToString();
            //    product.Price = i;
            //    product.Category = "category" + i.ToString();
            //    AddProduct(container, product);
            //}
            for (int k = 150; k < 160; k++) {
                ProductService.Models.Product p = new ProductService.Models.Product();

                p.Name = "test" + k.ToString();
                AddProduct(container, p);
            }
            ListAllProducts(container);
            //for (int i = 110; i < 120; i++) {
            //    DeleteProduct(container, new ProductService.Models.Product
            //    {
            //        Id = i
            //    });
            //}
            foreach(var p in container.Products)
            {
                DeleteProduct(container, p);
            }
        
            Console.ReadLine();
        }
    }
}
